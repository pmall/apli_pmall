# -*- coding: utf-8 -*-
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import pandas as pd
import sqlite3
from datetime import datetime, timedelta

from django.conf import settings

from django_plotly_dash import DjangoDash

from .utilities import *
import dashApp.utilities as utilities
from .app_components import SharedComponent, OxidativePotentialComponent

sc = SharedComponent()
opc = OxidativePotentialComponent()

BASE_VAR_SP = ["Date", "Particle size", "Station"]
BASE_VAR_SRC = ["Date", "Station", "Program"]
OP_VAR = [
    "PM10",
    "OP_AA_m3",  # "OP_AA_µg",
    "OP_DTT_m3",  # "OP_DTT_µg",
    "OP_DCFH_m3",  # "OP_DCFH_µg",
]

STATION_OP = ["Marnaz", "Passy", "Chamonix", "GRE-fr", "Talence", "Nice", "PdB"]

# =============================================================================

conn = sqlite3.connect(settings.BDDPM)  # BDDPM must be defined in local_settings.py
connOP = sqlite3.connect(settings.DB_OP)  # DB_OP must be defined in local_settings.py
df = pd.read_sql(
    'SELECT "{vars}" FROM OPvalues WHERE Station in (\'{stations}\')'.format(
        vars='", "'.join(BASE_VAR_SP+OP_VAR),
        stations="', '".join(STATION_OP)
    ),
    connOP
)
df["Date"] = pd.to_datetime(df["Date"])
dfmap = pd.read_sql('SELECT * FROM metadata_station WHERE abbrv IN ("{}");'.format(
    '", "'.join(STATION_OP),
), conn)
connOP.close()
conn.close()
# =============================================================================


# ==== STATION LIST ===========================================================
list_station = df["Station"].unique()
# list_station_OP = pd.read_sql(sqlquery_hasOP, con=conn)["Station"].tolist()
# ==== END STATION LIST =======================================================

notNumeric = [
    "index", "Number ID", "Date", "Sample ID", "Sample ID_PO", "Sample ID_chem",
    "Particle size", "Commentary", "Big serie", "Station", "Labels",
    "Programme",
]


SELECTEDSTATION = set()

# do not add species to plot when we already have too many...
tooManyPlot = 30
minSample = 40


app = DjangoDash("app_OP")

app.layout = html.Div(
    children=[
        html.Div(
            id="main",
            children=[
                # first column
                html.Div(
                    id="first-column",
                    children=[
                        html.Div([
                            sc.dropdown_component(
                                label="Station",
                                id="station-dropdown",
                                options=[{"label": l, "value": l} for l in
                                         list_station],
                                default=["GRE-fr"]
                            ),
                            sc.dropdown_component(
                                label="OP",
                                id="specie-dropdown",
                                default=["PM10", "OP_DTT_m3"]
                            ),
                        ]),
                        html.Div(
                            sc.get_daterange_slider(df["Date"]),
                            style={'margin': '0 20px 20px 20px'}
                        ),

                        sc.get_map_component(list_station),

                        # html.Div([
                        #     sc.datatable_component
                        # ]),
                        #
                        # html.Br(),

                        html.Div([
                            opc.gettinghelp_component
                        ])
                    ],
                ),
                # second column
                html.Div(
                    id="second-column",
                    children=[
                        sc.timeserie_component
                    ],
                )
            ],
            style={
                'width': '100%',
                'padding': '20px',
                'display': 'flex'
            }
        ),
    ]
)


external_css = [
    "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css",
    "/static/css/apps.css", # litle hack to serve django css assets
]

for css in external_css:
    app.css.append_css({"external_url": css})


@app.callback(Output('specie-dropdown', 'options'),
              [Input('station-dropdown', 'value')])
def set_specie_option(stations):
    species = []
    for station in stations:
        species = species + [i for i in
                             df[df["Station"]==station]\
                             .dropna(axis=1, how="all")\
                             .columns
                             if i not in species]
    species = set(species) - set(BASE_VAR_SP)
    species = list(species)
    species.sort()
    return [{'label': i, 'value': i} for i in species]


def update_selected_station(map4station, dropdown4station):
    """
    Update the SELECTEDSTATION variable according to both the map and the
    dropwdown list of station.
    """
    #TODO: lasso selection
    # if values1:
    #     for point in values1["points"]:
    #         s = point["text"]
    #         print(s)
    #         if s in SELECTEDSTATIONMAP:
    #             SELECTEDSTATIONMAP.remove(s)
    #         else:
    #             SELECTEDSTATIONMAP.append(s)

    # reset selected station and repopulate it
    if not map4station:
        SELECTEDSTATION.clear()
    # point selected on the map
    if map4station:
        for point in map4station["points"]:
            s = point["text"]
            if s in SELECTEDSTATION:
                SELECTEDSTATION.remove(s)
                dropdown4station.remove(s)
            else:
                SELECTEDSTATION.update([s])
    # add also station mannualy selected
    SELECTEDSTATION.update(dropdown4station)

    # no return. SELECTEDSTATION is a static variable


@app.callback(Output('station-dropdown', 'value'),
              [Input('map-graph', 'clickData')],
              [State('station-dropdown', 'value')])
def update_dropdown_station_selected(values, stations):
    update_selected_station(values, stations)
    return list(SELECTEDSTATION)


@app.callback(Output('map-graph', 'figure'),
              [Input('station-dropdown', 'value')],
              [State('map-graph', 'figure')])
def update_map_station_selected(stations, figure):
    update_selected_station(None, stations)
    # TODO: update_selected_station is called even if the stations variable is
    # unchanged.

    data = sc.get_map_data(list_station, SELECTEDSTATION)
    # idx = dfmap.abbrv.isin(SELECTEDSTATION)
    # data.append(
    #     dict(
    #         type = 'scattergeo',
    #         lon = dfmap.loc[idx,"longitude"],
    #         lat = dfmap.loc[idx,"latitude"],
    #         text = dfmap.loc[idx,"abbrv"],
    #         name = ""
    #     )
    # )
    figure = {"data": data, "layout": sc.get_map_layout()}
    return figure


@app.callback(Output('ts-graph', 'figure'),
              [
                  Input('specie-dropdown', 'value'),
                  Input('station-dropdown', 'value')
              ])
def update_ts_graph(species, stations):
    # print("CB for TS")
    traces = []
    sources = []

    to_return = {
        'data': traces,
        'layout': go.Layout(
            yaxis={"title": ' or '.join(get_units(species, sources))},
            showlegend=True,
            title="Time serie(s)",
            margin=go.layout.Margin(
                l=50,
                r=00,
                b=50,
                t=50,
                pad=0
            ),
            legend=dict(orientation="h")
        )
    }

    if len(species) == 0:
        return to_return

    nbPlot = 0 #len(stations) * len(set(species)-set(notNumeric))
    if nbPlot > tooManyPlot:
        print("TS: too many things to plot... skip it", nbPlot)
        return returnError

    dfdt = get_values(table="OPvalues", cols=species,
                      where="Station", isin=stations,
                      base_var=BASE_VAR_SP,
                      DBpath=settings.DB_OP
                     )
    # dfdt = pd.DataFrame(datatable)

    dfdt["Labels"] = pd.np.nan
    stations = dfdt["Station"].unique()
    # species = dfdt.columns

    dfdt["Labels"].fillna(value=pd.np.nan, inplace=True)

    # should never happen... but just in case
    if "Date" not in dfdt.columns:
        print("TS: no date given")
        return returnError

    species = set(species) - set(BASE_VAR_SP) - set(notNumeric)
    sources = set(sources) - set(BASE_VAR_SRC)

    for station in stations:
        dftmp = dfdt[dfdt["Station"]==station]

        # dftmp["Program PMF"].replace({"None", pd.np.nan}, inplace=True)
        for toplot, var in zip(("species", "sources"), (species, sources)):
            if len(var)==0:
                continue
            groupby = []
            if toplot == "species":
                if any(dftmp["Labels"].notnull()):
                    groupby += ["Labels"]
                groupby += ["Particle size"]
            elif toplot == "sources":
                groupby += ["Program PMF"]
            traces += [i for i in plot_ts(dftmp, station, var, groupby)]

    return {
        'data': traces,
        'layout': go.Layout(
            yaxis={"title": ' or '.join(get_units(species, sources))},
            showlegend=True,
            title="Time serie(s)",
            margin=go.layout.Margin(
                l=50,
                r=00,
                b=50,
                t=50,
                pad=0
            ),
            legend=dict(orientation="h")
        )
    }

@app.callback(Output('box-graph', 'figure'),
              [
                  Input('specie-dropdown', 'value'),
                  Input('station-dropdown', 'value'),
                  Input('tab-boxplot', 'value'),
                  Input('boxplot-options-graph-type', 'value'),
                  Input('boxplot-options-groupby', 'value')
              ])
def update_box_grah(species, stations, temporality, plots_options, groupby_var):
    """Seasonal boxplot or barplot"""
    traces = []
    sources = []
    returnError = {'date': traces,
                   'layout': {'title': 'Seasonal dispersion'}
                  }
    if len(species) == 0:
        return returnError

    dfdt = get_values(table="OPvalues", cols=species,
                      where="Station", isin=stations,
                      base_var=BASE_VAR_SP,
                      DBpath=settings.DB_OP
                     )
    # dfdt = pd.DataFrame(datatable)

    dfdt["Labels"] = pd.np.nan
    stations = dfdt["Station"].unique()

    species = set(species) - set(BASE_VAR_SP) - set(notNumeric)
    sources = set(sources) - set(BASE_VAR_SRC)

    nbPlot = len(stations) * len(set(species)-set(notNumeric))
    if nbPlot > tooManyPlot:
        print("TS: too many things to plot... skip it", nbPlot)
        return
    if (len(species) == 0) and (len(sources)==0):
        print("BOX: no data to plot, sp:", species)
        return returnError

    dfdt = utilities.add_month(dfdt, season=True)
    dfdt["year"] = dfdt["Date"].apply(lambda x: x.year)
    dfdt["all"] = "All date"
    dfdt["hot-cold"] = "MJJASO (Hot)"
    dfdt.loc[dfdt["month"].isin(["Nov", "Dec", "Jan", "Feb", "Mar", "Apr"]), "hot-cold"] = "NDJFMA (Cold)"

    # ==== Plot option ========================================================
    plot_type = "box" if "boxplot" in plots_options else "bar"
    # X variable
    if temporality == "season":
        date_var_list = ["DJF", "MAM", "JJA", "SON"]
    elif temporality == "month":
        date_var_list = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                         "Oct", "Nov", "Dec"]
    elif temporality == "year":
        date_var_list = [str(x) for x in list(range(dfdt["year"].min(), dfdt['year'].max()))]
    elif temporality == "all":
        date_var_list = ["All date"]
    elif temporality == "hot-cold":
        date_var_list = ["NDJFMA (Cold)", "MJJASO (Hot)"]
    else:
        print("???")

    x_var = groupby_var
    if x_var == "Date":
        x_var_col = temporality
        x_var_list = date_var_list
        hue_var_col = "Station"
        hue_var_list = stations
    elif x_var == "Site":
        x_var_col = "Station"
        x_var_list = stations
        hue_var_col = temporality
        hue_var_list = date_var_list
    xticklabels = x_var_list

    # ==== Plot part ==========================================================
    for hue in hue_var_list:
        dftmp = dfdt[dfdt[hue_var_col]==hue]
        for toplot, y_var in zip(("species", "sources"), (species, sources)):
            if len(y_var)==0 or pd.isnull(dftmp[list(y_var)]).all().all():
                continue
            groupby = []
            if toplot == "species":
                if any(dftmp["Labels"].notnull()):
                    groupby += ["Labels"]
                groupby += ["Particle size"]
            elif toplot == "sources":
                groupby += ["Program PMF"]
            traces += [
                i for i in plot_box(
                    dftmp, 
                    hue,
                    x_var=x_var_col,
                    y_var=y_var,
                    groupby=groupby,
                    plot_type=plot_type
                )
            ]

    return {
        'data': traces,
        'layout': go.Layout(
            yaxis={"title": " or ".join(get_units(species, sources))},
            xaxis={'categoryorder': 'array',
                   'categoryarray': xticklabels},
            showlegend=True,
            boxmode='group',
            margin=go.layout.Margin(
                l=50,
                r=00,
                b=50,
                t=50,
                pad=0
            ),
            legend=dict(orientation="h")
        )
    }


if __name__ == '__main__':
    app = _create_app()
    app.run_server(**server_setting)
