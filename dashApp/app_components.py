import sqlite3
from datetime import datetime
import pandas as pd
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
import dash_table as dt
import plotly.graph_objs as go
import plotly.express as px

from django.conf import settings


class SharedComponent:
    """Component shared by several app."""

    def get_layout(self, df, list_station=None):
        if list_station is None:
            conn = sqlite3.connect(
                settings.BDDPM
            )  # BDDPM must be defined in local_settings.py
            sqlquery = """
                SELECT DISTINCT Station FROM 
                (
                    SELECT Station FROM PMF_contributions
                    UNION ALL
                    SELECT Station FROM values_all
                )
                ORDER BY Station;
                """
            list_station = pd.read_sql(sqlquery, con=conn)["Station"].tolist()
            conn.close()

        layout = dbc.Container(
            id="container-main",
            fluid=True,
            children=[
                dbc.Row(
                    id="options-row",
                    children=[
                        dbc.Col(
                            self.dropdown_component(
                                label="Station",
                                id="station-dropdown",
                                options=[
                                    {"label": l, "value": l} for l in list_station
                                ],
                                default=[],
                            )
                        ),
                        dbc.Col(
                            self.dropdown_component(
                                label="Specie",
                                id="specie-dropdown",
                                default=[],
                            )
                        ),
                        dbc.Col(
                            self.dropdown_component(
                                label="PMF factor",
                                id="source-dropdown",
                                default=[],
                            ),
                        ),
                    ],
                ),
                dbc.Row(
                    dbc.Col(
                        children=[self.get_options_component(date=df["Date"])],
                    ),
                ),
                dbc.Row(
                    children=[
                        dbc.Col(  # first column
                            id="first-column",
                            width=5,
                            children=[
                                dbc.Row(
                                    [
                                        dbc.Col(
                                            children=self.get_map_component(
                                                list_station
                                            ),
                                        ),
                                    ]
                                ),
                                html.Div([self.datatable_component]),
                                html.Br(),
                                html.Div([self.gettinghelp_component]),
                            ],
                        ),  # end first column
                        dbc.Col(
                            id="second-column",
                            width=7,
                            children=[
                                dcc.Tabs(
                                    id="tab-2-col",
                                    value="1",
                                    children=[
                                        dcc.Tab(
                                            label="Time series",
                                            value="1",
                                            children=[self.timeserie_component],
                                        ),
                                        dcc.Tab(
                                            label="Scatter plot",
                                            value="2",
                                            children=[self.scatterplot_component],
                                        ),
                                        dcc.Tab(
                                            label="Podium plot",
                                            value="3",
                                            children=[self.podiumplot_component],
                                        ),
                                    ],
                                )
                            ],
                        ),  # end second column
                    ],
                ),  # end row
            ],
        )
        return layout

    def datetime2fractionaldate(self, date):
        """
        Convert a datetime object to fractional date (2012.7)
        """
        year = date.year
        boy = datetime(year, 1, 1)
        eoy = datetime(year + 1, 1, 1)
        result = year + (date - boy).total_seconds() / (eoy - boy).total_seconds()
        return result

    def get_daterange_slider(self, dates, vertical=False):
        years = sorted(dates.apply(lambda x: x.year).unique())
        slider = dcc.RangeSlider(
            id="date_minmax",
            min=dates.apply(lambda x: x.year).min(),
            max=dates.apply(lambda x: x.year).max() + 1,
            value=[
                self.datetime2fractionaldate(dates.min()),
                self.datetime2fractionaldate(dates.max()) + 1,
            ],
            step=1 / 12,
            marks={
                str(year): str(year)[-2:] for year in range(min(years), max(years) + 1)
            },
            vertical=vertical,
        )
        return slider

    def get_map_data(self, stations, selected=None):
        """Get the map data given by dfmap

        :dfmap: pandas DataFrame
        :returns: list of plotly trace
        """
        con = sqlite3.connect(settings.BDDPM)
        dfmap = pd.read_sql(
            'SELECT * FROM metadata_station WHERE abbrv in ("{stations}") OR name IN ("{stations}");'.format(
                stations='", "'.join(stations)
            ),
            con=con,
        )
        con.close()

        markers_typo = {"urban": "square", "periurban": "circle", "rural": "diamond"}
        # markers = dfmap["area type"].apply(lambda x: markers_typo[x])
        traces = []
        # for typo in dfmap["area type"].unique():
        #     idx = dfmap["area type"] == typo

        color = dfmap.loc[:, ["name", "abbrv"]]  # .to_frame()
        color["color"] = "black"
        if selected is not None:
            idx_selected = color["abbrv"].isin(selected) | color["name"].isin(selected)
            color.loc[idx_selected, "color"] = "orange"

        traces.append(
            dict(
                type="scattergeo",
                lon=dfmap.loc[:, "longitude"],
                lat=dfmap.loc[:, "latitude"],
                text=dfmap.loc[:, "name"],
                mode="markers",
                name="",
                marker=dict(symbol="square", size=10, color=color["color"]),
                # showlegend=False
            )
        )

        # add legend hack
        # for typo in markers_typo.keys():
        #     traces.append(
        #         dict(
        #             type='scattergeo',
        #             lon=[0],
        #             lat=[0],
        #             mode='markers',
        #             name=typo,
        #             marker=dict(
        #                 symbol=markers_typo[typo],
        #                 size=10,
        #                 color="black"
        #             ),
        #         )
        #     )

        return traces

    # def get_map_data(self, stations):
    #     """Get the map data given by dfmap
    #
    #     :dfmap: pandas DataFrame
    #     :returns: list of plotly trace
    #     """
    #     con = sqlite3.connect(settings.BDDPM)
    #     dfmap = pd.read_sql(
    #         "SELECT * FROM metadata_station WHERE abbrv in ('{stations}');".format(
    #             stations="', '".join(stations)
    #         ),
    #         con=con
    #     )
    #     con.close()
    #
    #     DATA_MAP = [dict(
    #         type='scattergeo',
    #         lon=dfmap['longitude'],
    #         lat=dfmap['latitude'],
    #         text=dfmap['abbrv'],
    #         mode='markers',
    #         name=''
    #         )
    #     ]
    #     return DATA_MAP

    def get_map_layout(self):
        """Return the layout for the map.

        :returns: dict
        """
        LAYOUT_MAP = dict(
            showlegend=False,
            margin=go.layout.Margin(l=20, r=20, b=20, t=20, pad=2),
            geo=dict(
                scope="world",
                showland=True,
                landcolor="rgb(212, 212, 212)",
                subunitcolor="rgb(255, 255, 255)",
                countrycolor="rgb(255, 255, 255)",
                showlakes=False,
                lakecolor="rgb(255, 255, 255)",
                showsubunits=True,
                showcountries=True,
                resolution=50,
                projection=dict(type="equirectangular", rotation=dict(lon=0)),
                lonaxis=dict(showgrid=True, gridwidth=0.5, range=[-5.0, 15.0], dtick=5),
                lataxis=dict(showgrid=True, gridwidth=0.5, range=[40.0, 55.0], dtick=5),
            ),
        )
        return LAYOUT_MAP

    def get_map_figure(self, stations, selected=None):
        con = sqlite3.connect(settings.BDDPM)
        dfmap = pd.read_sql(
            'SELECT * FROM metadata_station WHERE abbrv in ("{stations}") OR name in ("{stations}");'.format(
                stations='", "'.join(stations)
            ),
            con=con,
        )
        con.close()

        markers_typo = {"urban": "square", "periurban": "circle", "rural": "diamond"}
        # dfmap["markers"] = dfmap["area type"].apply(lambda x: markers_typo[x])
        dfmap["colors"] = "black"
        if selected is not None:
            dfmap.loc[dfmap["abbrv"].isin(selected), "colors"] = "orange"

        # fig = px.scatter_mapbox(
        fig = px.scatter_geo(
            dfmap,
            lat="latitude",
            lon="longitude",
            text="abbrv",
            color="colors",
        )
        # fig.update_traces(
        #     # marker_symbol="square",
        #     marker_size=5,
        #
        # )
        fig.update_layout(
            mapbox={
                "center": {"lon": 3, "lat": 47},
                "style": "open-street-map",
                "zoom": 4,
            },
            showlegend=False,
            margin=go.layout.Margin(l=20, r=20, b=20, t=20, pad=2),
            geo=dict(
                lonaxis=dict(showgrid=True, gridwidth=0.5, range=[-5.0, 15.0], dtick=5),
                lataxis=dict(showgrid=True, gridwidth=0.5, range=[40.0, 55.0], dtick=5),
            ),
        )
        return fig

    def get_map_component(self, stations):
        """TODO: Docstring for get_map_component.

        :dfmap: TODO
        :returns: TODO

        """
        # fig = self.get_map_figure(stations)
        map_component = html.Div(
            id="map-component",
            children=[
                dcc.Graph(
                    id="map-graph",
                    # figure=fig
                    figure={
                        "data": self.get_map_data(stations),
                        "layout": self.get_map_layout(),
                    },
                )
            ],
        )
        return map_component

    def dropdown_component(
        self, label, id, options=None, default=["ANDRA-PM10"], multi=True
    ):
        if options is None:
            options = {d: d for d in default}

        div = html.Div(
            children=[
                html.Label(label),
                dcc.Dropdown(id=id, options=options, multi=multi, value=default),
            ],
            style={"position": "relative"},
        )
        return div

    def get_options_component(self, date=None):
        if date is not None:
            slider = self.get_daterange_slider(date, vertical=False)
            slider_component = [dbc.Label("Date (year)"), slider]
        options_component = dbc.Row(
            children=[
                dbc.Col(
                    width=4,
                    children=slider_component if date is not None else "",
                ),
                dbc.Col(
                    width=3,
                    children=[
                        self.dropdown_component(
                            label="Particle size",
                            id="particle-size-dropdown",
                            options=[
                                {"label": "PM₁₀", "value": "PM10"},
                                {"label": "PM₂.₅", "value": "PM2.5"},
                                {"label": "PM₁", "value": "PM1"},
                                {"label": "Unknown", "value": "PMX"},
                            ],
                            default=["PM10", "PM2.5", "PM1", "PMX"],
                        )
                    ],
                ),
                dbc.Col(
                    children=[
                        dbc.Label("Options"),
                        dcc.Checklist(
                            id="options_check",
                            className="options-select-items",
                            options=[
                                {
                                    "label": "Use labels to group by",
                                    "value": "uselabel",
                                },
                            ],
                            value=["uselabel"],
                            labelStyle={"display": "inline-block"},
                        ),
                    ]
                ),
            ]
        )
        return options_component

    datatable_component = html.Div(
        id="datatable-component",
        children=[
            html.Div(
                dt.DataTable(
                    data=[{}],  # initialise the rows
                    sort_action="native",
                    sort_mode="multi",
                    filter_action="native",
                    editable=False,
                    export_columns="all",
                    export_format="xlsx",
                    # page_action="native",
                    # page_size=20,
                    fixed_rows={"headers": True, "data": 0},
                    virtualization=True,
                    page_action="none",
                    style_cell={
                        "whiteSpace": "normal",
                        "minWidth": "50px",
                    },
                    style_data_conditional=[
                        {"if": {"column_id": "Date"}, "width": "300px"},
                        # {'if': {'column_id': 'Particle_size'},
                        #  'width': '180px'},
                        {"if": {"column_id": "Station"}, "width": "150px"},
                    ],
                    id="datatable",
                ),
            ),
        ],
    )

    gettinghelp_component = html.Div(
        id="help-component",
        children=[
            dcc.Markdown(
                """
### Getting help

#### Dash
This app is written with [Dash](https://plot.ly/dash/), so the graph are
interactive and responsive. *Hover* over points to see their values, *click* on
legend items to toggle traces, *click and drag* to zoom, *hold down shift, and
click and drag* to pan.

#### This app

Check the [README file](https://gricad-gitlab.univ-grenoble-alpes.fr/pmall/apli_pmall)
on the UGA gitlab for an example on how to use this app.
"""
            )
        ],
    )

    boxplot_options = html.Div(
        id="boxplot-options",
        children=[
            html.Div(
                [
                    dbc.Label("Graph type"),
                    dbc.RadioItems(
                        id="boxplot-options-graph-type",
                        className="options-items",
                        options=[
                            {"label": "boxplot", "value": "boxplot"},
                            {"label": "barplot (mean)", "value": "barplot"},
                        ],
                        value="boxplot",
                        inline=True,
                    ),
                ],
                className="d-inline-block mr-4",
            ),
            html.Div(
                [
                    dbc.Label("Group by"),
                    dbc.RadioItems(
                        id="boxplot-options-groupby",
                        className="options-items",
                        options=[
                            {"label": "Date", "value": "Date"},
                            {"label": "Site", "value": "Site"},
                        ],
                        value="Date",
                        inline=True,
                    ),
                ],
                className="d-inline-block",
            ),
        ],
    )

    timeserie_component = html.Div(
        children=[
            # plots_options,
            html.Div(
                children=[
                    html.H4("Timeserie"),
                    html.Br(),
                    html.Div(
                        [
                            dbc.Label("Aggregate"),
                            dbc.RadioItems(
                                id="daily_mean",
                                className="options-items",
                                options=[
                                    {"label": "None", "value": "None"},
                                    {"label": "Daily mean", "value": "daily_mean"},
                                ],
                                value="daily_mean",
                                inline=True,
                            ),
                        ]
                    ),
                    dcc.Graph(
                        id="ts-graph",
                        figure=go.Figure(
                            {
                                "data": [],
                                "layout": {
                                    "title": "Select at least 1 station and variable"
                                },
                            }
                        ),
                        config={
                            "showSendToCloud": True,
                            "plotlyServerURL": "https://plot.ly",
                        },
                    ),
                ]
            ),
            html.Div(
                children=[
                    html.H4("Seasonnal aggregation"),
                    html.Br(),
                    dbc.Row(
                        children=[
                            dbc.Col(
                                width=4,
                                children=[
                                    dropdown_component(
                                        self=None,
                                        id="boxplot-aggregation",
                                        label="Seasonnal aggregation",
                                        options=[
                                            {"label": "All date", "value": "all"},
                                            {"label": "Annual", "value": "year"},
                                            {"label": "Hot/cold", "value": "hot-cold"},
                                            {"label": "Seasonal", "value": "season"},
                                            {"label": "Monthly", "value": "month"},
                                        ],
                                        multi=False,
                                        default="season",
                                    )
                                ],
                            ),
                            dbc.Col(children=boxplot_options),
                        ]
                    ),
                    dcc.Graph(
                        id="box-graph",
                        figure={"data": [], "layout": {"title": "Seasonal variation"}},
                        config={
                            "showSendToCloud": True,
                            "plotlyServerURL": "https://plot.ly",
                        },
                    ),
                ]
            ),
        ]
    )

    scatterplot_component = html.Div(
        children=[
            # select second specie
            html.Div(id="select-2-specie"),
            html.Div(
                children=[
                    html.Div(
                        [
                            dbc.Label("X and Y axis are :"),
                            dbc.RadioItems(
                                id="scatter_groupby",
                                className="options-items",
                                options=[
                                    {"label": "Sites", "value": "site"},
                                    {"label": "Species", "value": "specie"},
                                ],
                                value="site",
                                inline=True,
                            ),
                        ]
                    ),
                    dcc.Graph(
                        id="scatter-graph",
                        figure=go.Figure(
                            {"data": [], "layout": {"title": "Scatter plot"}}
                        ),
                    ),
                ]
            ),
        ]
    )

    podiumplot_component = html.Div(
        children=[
            dbc.Row(
                children=[
                    dbc.Col(
                        width=6,
                        children=[
                            dropdown_component(
                                self=None,
                                id="podium-col-primary",
                                label="Oxidative potential",
                                multi=False,
                                options=[
                                    {"label": "OP AA/m³", "value": "OP_AA_m3"},
                                    {"label": "OP DTT/m³", "value": "OP_DTT_m3"},
                                    {"label": "OP DCFH/m³", "value": "OP_DCFH_m3"},
                                    {"label": "OP AA/µg", "value": "OP_AA_µg"},
                                    {"label": "OP DTT/µg", "value": "OP_DTT_µg"},
                                    {"label": "OP DCFH/µg", "value": "OP_DCFH_µg"},
                                ],
                                default=None,
                            )
                        ],
                    ),
                    dbc.Col(
                        width=6,
                        children=[
                            dropdown_component(
                                self=None,
                                id="podium-col-secondary",
                                label="PM mass",
                                multi=False,
                                options=[
                                    {"label": "PM10", "value": "PM10"},
                                    {
                                        "label": "PM10 reconstruites",
                                        "value": "PM10recons",
                                    },
                                    {"label": "PM2.5", "value": "PM2.5"},
                                ],
                                default=None,
                            ),
                        ],
                    ),
                ]
            ),
            dcc.Graph(
                id="podium-graph",
                figure={"data": [], "layout": {"title": "Podium"}},
            ),
        ]
    )


class OxidativePotentialComponent:

    """Docstring for OxidativePotentialComponent."""

    gettinghelp_component = html.Div(
        id="help-component",
        children=[
            dcc.Markdown(
                """
#### Data availability

This app is intended to provide suplementary informations for the article of
[Calas et al. (2019)](https://www.mdpi.com/2073-4433/10/11/698) and presents the whole dataset gathered in this study.

If you want to re-use part or whole of this dataset, we will be pleased
to share it. But since the data were gathered from different
research program and in partly founded by the ANSES, the dataset is still
copyrighted, so please [contact us](/contact) before any use of these data.
                """
            )
        ],
    )


class SOURCESComponents:
    """Component for the SOURCES app"""

    def get_map_data(self, stations, selected=None):
        """Get the map data given by dfmap

        :dfmap: pandas DataFrame
        :returns: list of plotly trace
        """
        con = sqlite3.connect(settings.BDDSOURCES)
        dfmap = pd.read_sql(
            'SELECT * FROM metadata_station WHERE abbrv in ("{stations}") OR name in ("{stations}");'.format(
                stations='", "'.join(stations)
            ),
            con=con,
        )
        con.close()

        markers_typo = {
            "urban": "circle",
            "urban valley": "triangle-up",
            "industrial": "cross",
            "remote": "star",
            "traffic": "pentagon",
        }
        # markers = dfmap["area type"].apply(lambda x: markers_typo[x])
        traces = []
        for typo in dfmap["area type"].unique():
            idx = dfmap["area type"] == typo

            color = dfmap.loc[idx, ["abbrv", "name"]]  # .to_frame()
            color["color"] = "black"
            if selected is not None:
                idx_selected = color["abbrv"].isin(selected) | color["name"].isin(
                    selected
                )
                color.loc[idx_selected, "color"] = "orange"

            traces.append(
                dict(
                    type="scattergeo",
                    lon=dfmap.loc[idx, "longitude"],
                    lat=dfmap.loc[idx, "latitude"],
                    text=dfmap.loc[idx, "abbrv"],
                    mode="markers",
                    name=typo,
                    marker=dict(
                        symbol=markers_typo[typo], size=10, color=color["color"]
                    ),
                    showlegend=False,
                )
            )

        # add legend hack
        for typo in markers_typo.keys():
            traces.append(
                dict(
                    type="scattergeo",
                    lon=[0],
                    lat=[0],
                    mode="markers",
                    name=typo,
                    marker=dict(symbol=markers_typo[typo], size=10, color="black"),
                )
            )

        return traces

    def get_map_layout(self):
        """Return the layout for the map.

        :returns: dict
        """

        LAYOUT_MAP = dict(
            showlegend=True,
            margin=go.layout.Margin(l=00, r=00, b=00, t=00, pad=0),
            geo=dict(
                scope="world",
                showland=True,
                landcolor="rgb(212, 212, 212)",
                subunitcolor="rgb(255, 255, 255)",
                countrycolor="rgb(255, 255, 255)",
                showlakes=False,
                lakecolor="rgb(255, 255, 255)",
                showsubunits=True,
                showcountries=True,
                resolution=50,
                projection=dict(type="equirectangular", rotation=dict(lon=2.5)),
                lonaxis=dict(showgrid=True, gridwidth=0.5, range=[-5.0, 20.0], dtick=5),
                lataxis=dict(showgrid=True, gridwidth=0.5, range=[40.0, 55.0], dtick=5),
            ),
            legend=dict(
                orientation="v",
                x=-0.1,
                y=1,
                font=dict(size=16),
            ),
        )
        return LAYOUT_MAP

    def get_map_component(self, stations):
        """TODO: Docstring for get_map_component.

        :dfmap: TODO
        :returns: TODO

        """
        map_component = html.Div(
            id="map-component",
            children=[
                dcc.Graph(
                    id="map-graph",
                    figure={
                        "data": self.get_map_data(stations),
                        "layout": self.get_map_layout(),
                    },
                )
            ],
        )
        return map_component

    gettinghelp_component = html.Div(
        children=[
            dcc.Markdown(
                """
### The SOURCES programme

This app is the interactive visualization of the SOURCES programme, founded by the
ADEME, conduct by
[IGE](https://www.ige-grenoble.fr) and [INERIS](https://www.ineris.fr). 
Please refer to the
[report (fr)](https://www.lcsqa.org/fr/rapport/2016/ineris/traitement-harmonise-jeux-donnees-multi-sites-etude-sources-pm-positive-matrix-f)
or the [article (en)](https://www.mdpi.com/2073-4433/10/6/310)) for a detailed discussion.

The app is developped and maintained by Samuël Weber.

### Dash

This app is written with [Dash](https://plot.ly/dash/), so the graph are
interactive and responsive. *Hover* over points to see their values, *click* on
legend items to toggle traces, *click and drag* to zoom, *hold down shift, and
click and drag* to pan.
"""
            )
        ],
        style={"display": "block", "width": "100%", "vertical-align": "top"},
    )

    profile_component = html.Div(
        id="tab-concentration",
        children=[
            dcc.Graph(
                id="concentration-graph",
                figure={"data": [], "layout": {"title": "Relative concentration"}},
            ),
            dcc.Graph(
                id="totalspeciesum-graph",
                figure={
                    "data": [],
                    "layout": {"title": "Contribution to total specie sum"},
                },
            ),
        ],
    )

    uncertainty_component = html.Div(
        id="uncertainty-component",
        children=[
            dcc.Graph(
                id="uncertainty-graph-conc",
                figure={"data": [], "layout": {"title": "Uncertainties"}},
            ),
            dcc.Graph(
                id="uncertainty-graph-norm",
                figure={"data": [], "layout": {"title": "Uncertainties"}},
            ),
        ],
    )

    deltatool_component = html.Div(
        children=[
            html.Div(
                children=[
                    html.Label("Display: "),
                    dcc.RadioItems(
                        id="deltatool-options",
                        className="options-items",
                        options=[
                            {"label": "individual values", "value": "all"},
                            {"label": "only mean±std", "value": "mean"},
                        ],
                        value="all",
                    ),
                    dcc.Graph(
                        id="deltatool-graph",
                        figure={
                            "data": [],
                            "layout": {
                                "title": "DeltaTool distance",
                                "shapes": [
                                    {
                                        "type": "rect",
                                        "x0": 0,
                                        "y0": 0,
                                        "x1": 1,
                                        "y1": 0.6,
                                        "line": {
                                            "width": 0,
                                        },
                                        "fillcolor": "rgba(0, 255, 0, 0.1)",
                                    }
                                ],
                            },
                        },
                    ),
                ]
            ),
            html.Div(
                html.Div(
                    id="deltatool-description",
                    children=[
                        dcc.Markdown(
                            """
### Deltatool-like plot
The PD (Pearson distance) and SID (standardized identity distance) are two
metrics to evaluate the similarity between two profile. Here, every possible
pairs of profile are compared when they belong to the same category.

The green rectangle highlights the "acceptable area" PD < 0.4 and SID < 1 as
suggested by [Pernigotti & Belis
(2018)](http://fairmode.jrc.ec.europa.eu/document/fairmode/WG3/DeltaSA_tool_for_source_apportionment.pdf).
                                """
                        )
                    ],
                )
            ),
        ]
    )

    specie_repartition_component = html.Div(
        id="specie-repartition-component",
        children=[
            dcc.Graph(
                id="repartition-graph",
                figure={
                    "data": [],
                    "layout": {
                        "title": "Specie repartition among factors (Loading...)",
                    },
                },
            )
        ],
    )
