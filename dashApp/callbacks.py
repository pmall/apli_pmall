import sqlite3
import pandas as pd
import numpy as np
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
import plotly.graph_objs as go
from plotly.subplots import make_subplots
import plotly.express as px

from django.conf import settings

# from .server import app

from django_plotly_dash import DjangoDash

import dashApp.utilities as utilities
from .app_components import SharedComponent

sc = SharedComponent()

notNumeric = [
    "Index",
    "Number_ID",
    "Date",
    "Sample_ID",
    "Sample_ID_PO",
    "Sample_ID_chem",
    "Particle_size",
    "Commentary",
    "Big_serie",
    "Station",
    "Labels",
    "Program",
]

STATIONS_SHORT2LONG = {
    "LEN": "Lens",
    "POI": "Poitiers",
    "CHAM": "Chamonix",
    "LYN": "Lyon",
    "NIC": "Nice",
    "AIX": "Aix-en-provence",
    "RBX": "Roubaix",
    "REV": "Revin",
    "TAL": "Talence",
    "ROU": "Rouen",
    "NGT": "Nogent",
}

STATIONS_LONG2SHORT = {v: k for k, v in STATIONS_SHORT2LONG.items()}

BASE_VAR_SP = ["Date", "Particle_size", "Station"]
BASE_VAR_SRC = ["Date", "Station", "Program"]

SELECTEDSTATION = set()

# do not add species to plot when we already have too many...
tooManyPlot = 30
minSample = 40


def register_callbacks(app):
    @app.callback(
        Output("station-dropdown", "options"), [Input("particle-size-dropdown", "value")]
    )
    def set_station_option(particle_size):
        """
        Limit the station list to the ones with > X samples
        """
        stations_tmp = app.list_station.copy()

        sql_query = """
        SELECT Station, Particle_size 
        FROM values_all
        GROUP BY Station, Particle_size;
        """

        conn = sqlite3.connect(settings.BDDPM)
        df = pd.read_sql(sql_query, con=conn)
        conn.close()

        if None in df["Particle_size"].unique():
            df["Particle_size"] = df["Particle_size"].fillna("?")
        df = df.loc[df["Particle_size"].isin(particle_size)]

        station_dropdown = [
            {"label": s, "value": s}
            for s in stations_tmp
            if df["Station"].isin([s]).any()
        ]

        return station_dropdown

    @app.callback(
        Output("specie-dropdown", "options"), [Input("station-dropdown", "value")]
    )
    def set_specie_option(stations):
        species = []

        if len(stations) == 0:
            return [{"label": "", "value": ""}]

        conn = sqlite3.connect(settings.BDDPM)
        sql_query = """
        SELECT * FROM values_all WHERE Station IN ("{stations}")
        """.format(
            stations='", "'.join(stations)
        )
        df = (
            pd.read_sql(sql_query, con=conn)
            .replace("nan", np.nan)
            .dropna(axis=1, how="all")
        )
        conn.close()

        for station in stations:
            species = species + [
                i
                for i in df[df["Station"] == station].dropna(axis=1, how="all").columns
                if i not in species
            ]

        species = set(species) - set(BASE_VAR_SP)
        if hasattr(app, "limited_species"):
            species = species.intersection(set(app.limited_species))
        species = list(species)
        species.sort()
        return [{"label": i, "value": i} for i in species]

    # @app.callback(Output('specie-dropdown', 'value'),
    #               [Input('station-dropdown', 'value')],
    #               [State('specie-dropdown', 'value')])
    # def remove_species(stations, species):
    #     print(stations)
    #     if len(stations)==0:
    #         return []
    #     return species

    @app.callback(
        Output("source-dropdown", "options"), [Input("station-dropdown", "value")]
    )
    def set_source_option(stations):
        """Get all possible source given the stations"""
        sources = []
        sql = 'SELECT * FROM PMF_contributions WHERE station IN ("{stations}")'
        stations_mapped = [STATIONS_LONG2SHORT.get(s, s) for s in stations]
        sql = sql.format(stations='", "'.join(stations_mapped))
        dftmp = pd.read_sql(sql, con=sqlite3.connect(settings.BDDPM)).dropna(
            axis=1, how="all"
        )
        sources = list(set(dftmp.columns) - set(BASE_VAR_SRC) - set(["index"]))
        sources.sort()
        return [{"label": i, "value": i} for i in sources]

    def update_selected_station(map4station, dropdown4station):
        """
        Update the SELECTEDSTATION variable according to both the map and the
        dropwdown list of station.
        """
        # TODO: lasso selection
        # if values1:
        #     for point in values1["points"]:
        #         s = point["text"]
        #         print(s)
        #         if s in SELECTEDSTATIONMAP:
        #             SELECTEDSTATIONMAP.remove(s)
        #         else:
        #             SELECTEDSTATIONMAP.append(s)

        # reset selected station and repopulate it
        if not map4station:
            SELECTEDSTATION.clear()
        # point selected on the map
        if map4station:
            for point in map4station["points"]:
                s = point["text"]
                if s in SELECTEDSTATION:
                    SELECTEDSTATION.remove(s)
                    dropdown4station.remove(s)
                else:
                    SELECTEDSTATION.update([s])
        # add also station mannualy selected
        SELECTEDSTATION.update(dropdown4station)

        # no return. SELECTEDSTATION is a static variable

    @app.callback(
        Output("station-dropdown", "value"),
        [Input("map-graph", "clickData")],
        [State("station-dropdown", "value")],
    )
    def update_dropdown_station_selected(values, stations):
        update_selected_station(values, stations)
        return list(SELECTEDSTATION)

    @app.callback(
        Output("map-graph", "figure"),
        [Input("station-dropdown", "value")],
        [State("map-graph", "figure")],
    )
    def update_map_station_selected(stations, figure):
        update_selected_station(None, stations)
        # TODO: update_selected_station is called even if the stations variable is
        # unchanged.

        data = sc.get_map_data(app.list_station, SELECTEDSTATION)
        # idx = dfmap.abbrv.isin(SELECTEDSTATION)
        # data.append(
        #     dict(
        #         type = 'scattergeo',
        #         lon = dfmap.loc[idx,"longitude"],
        #         lat = dfmap.loc[idx,"latitude"],
        #         text = dfmap.loc[idx,"abbrv"],
        #         name = ""
        #     )
        # )
        figure = {"data": data, "layout": sc.get_map_layout()}
        # figure = sc.get_map_figure(app.list_station, SELECTEDSTATION)
        return figure

    @app.callback(
        Output("datatable", "data"),
        [
            Input("station-dropdown", "value"),
            Input("specie-dropdown", "value"),
            Input("source-dropdown", "value"),
            Input("particle-size-dropdown", "value"),
            Input("options_check", "value"),
            Input("date_minmax", "value"),
        ],
    )
    def update_datatable(
        stations, species, sources, particle_size, options, years_range
    ):
        """
        For user selections, return the relevant table
        """
        print("CB for Datatable")
        dftmp = pd.DataFrame(data={})
        if (len(species) + len(sources)) == 0:
            print("DT: no specie nor sources")
            return dftmp.to_dict("records")
        if len(stations) == 0:
            print("DT: no station")
            return dftmp.to_dict("records")

        stations_tmp = []
        datestart = utilities.fractionaldate2datetime(years_range[0])
        dateend = utilities.fractionaldate2datetime(years_range[1])

        stations_tmp = stations

        if "uselabel" in options and "Labels" not in species:
            species += ["Labels"]

        if len(stations_tmp) == 0:
            return dftmp.to_dict("records")

        if len(species) >= 1:
            sp = utilities.get_values(
                table="values_all",
                cols=species,
                where="Station",
                isin=stations,
                base_var=BASE_VAR_SP,
            )
            if None in sp["Particle_size"].unique():
                sp["Particle_size"] = sp["Particle_size"].fillna("?")
            sp = sp.loc[sp["Particle_size"].isin(particle_size)]
        else:
            sp = pd.DataFrame(columns=BASE_VAR_SP)

        if len(sources) != 0:
            pmf = utilities.get_PMF_contrib(
                sources,
                where="Station",
                isin=[STATIONS_LONG2SHORT.get(s, s) for s in stations_tmp],
                base_var=BASE_VAR_SRC,
            )
        else:
            pmf = pd.DataFrame(columns=["Date", "Station", "Program_PMF"])

        dftmp = pd.merge(sp, pmf, on=["Date", "Station"], how="outer")

        # dftmp["Program_PMF"].fillna(value="None", inplace=True)

        subset = [x for x in species if x not in BASE_VAR_SP] + sources
        first_and_last_values = dftmp.dropna(subset=subset, how="all")["Date"]

        if datestart is None:
            datestart = pd.datetime(2000, 1, 1)
        if dateend is None:
            dateend = pd.datetime.today()

        # squeeze data to first and last value (datewise)
        if datestart < first_and_last_values.min():
            datestart = first_and_last_values.min()
        if dateend > first_and_last_values.max():
            dateend = first_and_last_values.max()

        dftmp = dftmp[(datestart <= dftmp["Date"]) & (dftmp["Date"] <= dateend)]

        return dftmp.to_dict("records")

    @app.callback(
        Output("datatable", "columns"),
        [
            Input("station-dropdown", "value"),
            Input("specie-dropdown", "value"),
            Input("source-dropdown", "value"),
            Input("options_check", "value"),
        ],
    )
    def update_datatable_columns(stations, species, sources, options):
        """
        Update columns
        """
        default_col = BASE_VAR_SP.copy()
        if len(sources) > 0:
            default_col += ["Program_PMF"]
        col = default_col + [i for i in species + sources if i not in default_col]
        if "uselabel" in options:
            col += ["Labels"]
        columns = [{"name": i, "id": i} for i in col]
        return columns

    @app.callback(
        Output("ts-graph", "figure"),
        [Input("datatable", "derived_virtual_data"), Input("daily_mean", "value")],
        [
            State("specie-dropdown", "value"),
            State("source-dropdown", "value"),
            State("options_check", "value"),
        ],
    )
    def update_ts_graph(datatable, daily_mean, species, sources, options):
        print("CB for TS")
        traces = []

        figure = go.Figure(
            {
                "data": traces,
                "layout": dict(
                    yaxis={"title": " or ".join(utilities.get_units(species, sources))},
                    showlegend=True,
                    title="Time serie(s)",
                    margin=go.layout.Margin(l=50, r=00, b=50, t=50, pad=0),
                    legend=dict(orientation="h"),
                ),
            }
        )

        if datatable is None or len(datatable) == 0 or len(datatable[0]) == 0:
            figure["layout"]["title"] = "Select at least 1 station and variable"
            return figure

        nbPlot = 0  # len(stations) * len(set(species)-set(notNumeric))
        if nbPlot > tooManyPlot:
            print("TS: too many things to plot... skip it", nbPlot)
            return figure

        dfdt = pd.DataFrame(datatable)

        if "uselabel" not in options:
            dfdt["Labels"] = np.nan
        stations = dfdt["Station"].unique()
        # species = dfdt.columns

        dfdt["Labels"].fillna(value=np.nan, inplace=True)

        # should never happen... but just in case
        if "Date" not in dfdt.columns:
            print("TS: no date given")
            return figure

        species = set(species) - set(BASE_VAR_SP) - set(notNumeric)
        sources = set(sources) - set(BASE_VAR_SRC)

        daily_mean = {"None": False, "daily_mean": True}[daily_mean]

        for station in stations:
            dftmp = dfdt[dfdt["Station"] == station]

            dftmp["Program_PMF"].replace({"None", np.nan}, inplace=True)
            for toplot, var in zip(("species", "sources"), (species, sources)):
                if len(var) == 0:
                    continue
                groupby = []
                if toplot == "species":
                    if any(dftmp["Labels"].notnull()):
                        groupby += ["Labels"]
                    groupby += ["Particle_size"]
                elif toplot == "sources":
                    groupby += ["Program_PMF"]
                traces += [
                    i
                    for i in utilities.plot_ts(dftmp, station, var, groupby, daily_mean)
                ]

        for trace in traces:
            figure.add_trace(trace)

        figure.update_layout(
            yaxis={"title": " or ".join(utilities.get_units(species, sources))},
        )

        return figure

    @app.callback(
        Output("box-graph", "figure"),
        [
            Input("datatable", "derived_virtual_data"),
            Input("boxplot-aggregation", "value"),
            Input("boxplot-options-graph-type", "value"),
            Input("boxplot-options-groupby", "value"),
        ],
        [
            State("specie-dropdown", "value"),
            State("source-dropdown", "value"),
            State("options_check", "value"),
        ],
    )
    def update_box_graph(
        datatable, temporality, plots_options, groupby_var, species, sources, options
    ):
        """Seasonal boxplot or barplot"""
        traces = []
        figure = go.Figure(
            {
                "data": traces,
                "layout": dict(
                    title="Seasonal dispersion",
                    showlegend=True,
                    boxmode="group",
                    margin=go.layout.Margin(l=50, r=00, b=50, t=50, pad=0),
                    legend=dict(orientation="h"),
                ),
            }
        )
        if datatable is None or len(datatable) == 0 or len(datatable[0]) == 0:
            return figure

        dfdt = pd.DataFrame(datatable)

        if "uselabel" not in options:
            dfdt["Labels"] = np.nan
        stations = dfdt["Station"].unique()

        species = set(species) - set(BASE_VAR_SP) - set(notNumeric)
        sources = set(sources) - set(BASE_VAR_SRC)

        nbPlot = len(stations) * len(set(species) - set(notNumeric))
        if nbPlot > tooManyPlot:
            print("TS: too many things to plot... skip it", nbPlot)
            return
        if (len(species) == 0) and (len(sources) == 0):
            print("BOX: no data to plot, sp:", species)
            return figure

        dfdt = utilities.add_month(dfdt, season=True)
        dfdt["year"] = dfdt["Date"].apply(lambda x: x.year)
        dfdt["all"] = "All date"
        dfdt["hot-cold"] = "MJJASO (Hot)"
        dfdt.loc[
            dfdt["month"].isin(["Nov", "Dec", "Jan", "Feb", "Mar", "Apr"]), "hot-cold"
        ] = "NDJFMA (Cold)"

        # ==== Plot option ========================================================
        plot_type = "box" if "boxplot" in plots_options else "bar"
        # X variable
        print(temporality)
        if temporality == "season":
            date_var_list = ["DJF", "MAM", "JJA", "SON"]
        elif temporality == "month":
            date_var_list = [
                "Jan",
                "Feb",
                "Mar",
                "Apr",
                "May",
                "Jun",
                "Jul",
                "Aug",
                "Sep",
                "Oct",
                "Nov",
                "Dec",
            ]
        elif temporality == "year":
            date_var_list = [
                x for x in list(range(dfdt["year"].min(), dfdt["year"].max()))
            ]
        elif temporality == "all":
            date_var_list = ["All date"]
        elif temporality == "hot-cold":
            date_var_list = ["NDJFMA (Cold)", "MJJASO (Hot)"]
        else:
            print("???")

        x_var = groupby_var
        if x_var == "Date":
            x_var_col = temporality
            x_var_list = date_var_list
            hue_var_col = "Station"
            hue_var_list = stations
        elif x_var == "Site":
            x_var_col = "Station"
            x_var_list = stations
            hue_var_col = temporality
            hue_var_list = date_var_list
        xticklabels = x_var_list

        # ==== Plot part ==========================================================
        for hue in hue_var_list:
            dftmp = dfdt[dfdt[hue_var_col] == hue]
            for toplot, y_var in zip(("species", "sources"), (species, sources)):
                if len(y_var) == 0 or pd.isnull(dftmp[list(y_var)]).all().all():
                    continue
                groupby = []
                if toplot == "species":
                    if any(dftmp["Labels"].notnull()):
                        groupby += ["Labels"]
                    groupby += ["Particle_size"]
                elif toplot == "sources":
                    groupby += ["Program_PMF"]
                traces += [
                    i
                    for i in utilities.plot_box(
                        dftmp,
                        hue,
                        x_var=x_var_col,
                        y_var=y_var,
                        groupby=groupby,
                        plot_type=plot_type,
                    )
                ]

        for trace in traces:
            figure.add_trace(trace)

        figure.update_layout(
            yaxis={"title": " or ".join(utilities.get_units(species, sources))},
            xaxis={"categoryorder": "array", "categoryarray": xticklabels},
        )

        return figure

    @app.callback(
        Output("scatter-graph", "figure"),
        [
            Input("datatable", "derived_virtual_data"),
            Input("tab-2-col", "value"),
            Input("scatter_groupby", "value"),
        ],
        [
            State("specie-dropdown", "value"),
            State("source-dropdown", "value"),
            State("options_check", "value"),
        ],
    )
    def update_scatter_graph(datatable, tabselected, xy, species, sources, options):
        """Update the scatter graph

        :datatable: TODO
        :tabSelected: TODO
        :species: TODO
        :sources: TODO
        :options: TODO
        :returns: TODO

        """
        data = []
        figure = go.Figure(
            {
                "data": data,
                "layout": dict(
                    title="Scatterplot",
                    xaxis={"title": " or ".join(utilities.get_units(species, sources))},
                    yaxis={"title": " or ".join(utilities.get_units(species, sources))},
                    margin=go.layout.Margin(l=50, r=00, b=50, t=50, pad=0),
                    legend=dict(orientation="h", y=-0.2),
                    showlegend=True,
                    hovermode="closest",
                ),
            }
        )

        if tabselected != "2":
            return figure

        if datatable is None or len(datatable) == 0:
            figure["layout"]["title"] = "Select at least 2 variables or 2 stations"
            return figure

        dfdt = pd.DataFrame(datatable)

        if "uselabel" not in options:
            dfdt["Labels"] = np.nan
        stations = list(dfdt["Station"].unique())

        if xy == "site":
            hue = "specie"
        elif xy == "specie":
            hue = "site"

        if xy == "site":
            if len(stations) != 2:
                figure["layout"]["title"] = "Please select 2 stations"
                return figure
        elif xy == "specie":
            if len(species + sources) != 2:
                figure["layout"]["title"] = "Please select 2 species"
                return figure

        # species = set(species) - set(BASE_VAR_SP) - set(notNumeric)
        # sources = set(sources) - set(BASE_VAR_SRC)
        variables = species + sources

        if xy == "site":
            df = dfdt[["Date", "Station"] + variables]
            df = (
                df.pivot_table(index=["Date"], columns=["Station"], values=variables)
                .reset_index()
                .melt(id_vars=["Date"])
                .dropna()
            )
            df.columns = ["Date", "Specie", "Station", "value"]
            dfdt = df.pivot_table(
                index=["Date", "Specie"], columns=["Station"]
            ).reset_index()
            dfdt.columns = ["Date", "Specie"] + list(
                dfdt.columns.get_level_values("Station")[-2:]
            )

        else:
            dfdt = dfdt[variables + ["Station", "Date"]].dropna()

        x, y = stations if hue == "specie" else variables
        color = "Specie" if hue == "specie" else "Station"
        orders = variables if hue == "specie" else stations

        # Plot part ==============================================================
        figure = px.scatter(
            dfdt,
            x=x,
            y=y,  # variables[0], y=variables[1],
            opacity=0.5,
            color=color,  # color="Station",
            hover_data=["Date"],
            category_orders={color: orders},  # category_orders={"Station": stations},
            trendline="ols",
            marginal_x="histogram",
            marginal_y="histogram",
            # labels={"Station": ''}
        )

        xtitle = "{var} ({unit})".format(
            var=x,
            unit=" or ".join(
                utilities.get_units([s for s in variables], sources=len(sources) > 0)
            ),
        )
        ytitle = "{var} ({unit})".format(
            var=y,
            unit=" or ".join(
                utilities.get_units([s for s in variables], sources=len(sources) > 0)
            ),
        )

        figure.update_layout(xaxis_title=xtitle, yaxis_title=ytitle)

        return figure

    @app.callback(
        Output("podium-graph", "figure"),
        [
            # Input("datatable", "derived_virtual_data"),
            Input("tab-2-col", "value"),
            Input("station-dropdown", "value"),
            Input("particle-size-dropdown", "value"),
            Input("podium-col-primary", "value"),
            Input("podium-col-secondary", "value"),
        ],
    )
    # def update_podium_graph(datatable, tabselected, stations):
    def update_podium_graph(tabselected, stations, particle_size, col_primary, col_secondary):
        """Update the scatter graph

        :datatable: TODO
        :tabSelected: TODO
        :stations: TODO
        :returns: TODO

        """
        data = []
        figure = go.Figure(
            {
                "data": data,
                "layout": dict(
                    title="Scatterplot",
                    xaxis={"title": "Station"},
                    yaxis={"title": "µg/m³"},
                    margin=go.layout.Margin(l=50, r=00, b=50, t=50, pad=0),
                    legend=dict(orientation="h", y=-0.2),
                    showlegend=True,
                    hovermode="closest",
                ),
            }
        )

        if tabselected != "3":
            return figure

        if not stations:
            figure["layout"]["title"] = "Select at least 1 station"
            return figure
        if col_primary is None:
            figure["layout"]["title"] = "Select an OP variable"
            return figure
        if col_secondary is None:
            figure["layout"]["title"] = "Select a Mass variable"
            return figure

        df = utilities.get_values(
            table="values_all",
            cols=["Date", "Station", "Particle_size", col_primary, col_secondary],
            where="Station",
            isin=stations,
        )

        if df[col_primary].dropna().size == 0:
            figure["layout"]["title"] = f"No {col_primary} for the stations"
            return figure
        if df[col_secondary].dropna().size == 0:
            figure["layout"]["title"] = f"No {col_secondary} for the stations"
            return figure

        col_selected_pm_particle_size = "PM10" if col_secondary.startswith("PM10") else "PM2.5"

        if col_selected_pm_particle_size in df["Particle_size"].unique():
            df = df.loc[df["Particle_size"] == col_selected_pm_particle_size]
        else:
            figure["layout"]["title"] = f"No particle size {col_selected_pm_particle_size}"
            return figure

        df = df.groupby("Station").describe().reindex(stations).reset_index()

        figure = make_subplots(specs=[[{"secondary_y": True}]])
        figure.add_trace(
            go.Bar(
                x=df["Station"],
                y=df[(col_primary, "mean")],
                name="OP",
                marker=dict(color="lightgrey"),
            ),
            secondary_y=False,
        )
        figure.add_trace(
            go.Scatter(
                x=df["Station"],
                y=df[(col_secondary, "mean")],
                name="PM mass",
                mode="markers",
                marker=dict(
                    size=max(20 - len(stations), 15),
                    color="black"
                    ),
            ),
            secondary_y=True,
        )

        # Add figure title
        figure.update_layout(
            title_text=f"Ranked OP ({col_primary}) and mass ({col_secondary}) contribution"
        )

        df = df.replace({np.nan: 0})
        xticklabels = (
                "<b>"+df["Station"]+"</b>"
                + "<br>(n="
                + df[(col_primary, 'count')].apply(int).apply(str)
                + ")"
                ).values

        # Set x-axis title
        figure.update_xaxes(
                title_text="",
                tickvals=list(range(0, len(stations))),
                tickmode="array",
                ticktext=xticklabels
                )

        # Set y-axes titles
        grid_options = dict(showgrid=False)
        figure.update_yaxes(
            title_text=f"<b>OP</b> ({' or '.join(utilities.get_units([col_primary]))})",
            secondary_y=False,
            rangemode="tozero",
            **grid_options,
        )
        figure.update_yaxes(
            title_text="<b>Mass</b> (µg/m³)",
            secondary_y=True,
            rangemode="tozero",
            **grid_options,
        )

        return figure
