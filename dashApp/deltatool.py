import pandas as pd

def sourcesColor(s):
    colors = {
        'Aged seasalt': "#00b0ff",
        'Aged seasalt/HFO': "#8c564b",
        'Biomass burning': "#92d050",
        'Dust': "#dac6a2",
        'Fresh seasalt': "#00b0f0",
        'HFO': "#70564b",
        'HFO (stainless)': "#8c564b",
        'Industries': "#7030a0",
        'Mineral dust': "#dac6a2",
        'Nitrate rich': "#ff7f2a",
        'Primary biogenic': "#ffc000",
        'Resuspended dust': "#dac6a2",
        'Road salt': "#00b0f0",
        'Road traffic': "#000000",
        'Seasalt': "#00b0f0",
        'Secondary biogenic': "#8c564b",
        'Secondary biogenic/HFO': "#8c564b",
        'Sulfate rich': "#ff2a2a",
        'Sulfate rich/HFO': "#ff2a2a",
        'Anthropogenic SOA': "#8c564b",
    }
    if s not in colors.keys():
        print("WARNING: no {} found in colors".format(s))
        return "#666666"
    return colors[s]

# def compute_SID(df1, df2, source1, source2=None, isRelativeMass=True):
#     """
#     Compute the SID of the sources `source1` and `source2` in the profile
#     `df1`and `df2`. 
#     """
#     if not(source2):
#         source2 = source1
#     if source1 not in df1.dropna(axis=1, how="all").columns:
#         return np.nan
#     if source2 not in df2.dropna(axis=1, how="all").columns:
#         return np.nan
#     if not isRelativeMass:
#         p1 = to_relativeMass(df1.loc[:, source1])
#         p2 = to_relativeMass(df2.loc[:, source2])
#     else:
#         p1 = df1.loc[:, source1]
#         p2 = df2.loc[:, source2]
#     sp = p1.index.intersection(p2.index)
#     if len(sp)>3:
#         ID = pd.np.abs(p1[sp]-p2[sp])
#         MAD = p1[sp] + p2[sp]
#         SID = pd.np.sqrt(2)/len(sp) * (ID/MAD).sum()
#     else:
#         SID = pd.np.nan
#     return SID

# def compute_PD(df1, df2, source1, source2=None, isRelativeMass=True):
#     """
#     Compute the PD of the sources `source1` and `source2` in the profile
#     `df1`and `df2`. 
#     """
#     if not(source2):
#         source2 = source1
#     if source1 not in df1.dropna(axis=1, how="all").columns:
#         return np.nan
#     if source2 not in df2.dropna(axis=1, how="all").columns:
#         return np.nan
#     if not isRelativeMass:
#         p1 = to_relativeMass(df1.loc[:, source1])
#         p2 = to_relativeMass(df2.loc[:, source2])
#     else:
#         p1 = df1.loc[:, source1]
#         p2 = df2.loc[:, source2]
#     p1.dropna(inplace=True)
#     p2.dropna(inplace=True)
#     sp = p1.index.intersection(p2.index)
#     if len(sp)>3:
#         PD = 1 - pd.np.corrcoef(p1[sp], p2[sp])[1,0]**2
#     else:
#         PD = pd.np.nan
#     return PD

# def plot_deltatool_pretty(ax):
#     """
#     Format the given ax to conform with the "deltatool-like" visualization.
#     """
#     rect = mpatch.Rectangle((0, 0), width=1, height=0.4, facecolor="green", alpha=0.2, zorder=-1)
#     ax.add_patch(rect)
#     ax.set_xlim(0, 1.5)
#     ax.set_ylim(0, 1)
#     ax.set_xlabel("SID")
#     ax.set_ylabel("PD")

# def plot_similarityplot(PMF_profile, station1, station2, source1, source2=None,
#                         isRelativeMass=False, ax=None, plot_kw={}):
#     """
#     Plot the distance in the SID/PD space of 2 profiles for 2 stations.
#     """
#     if not source2:
#         source2 = source1
#
#     for station, source in itertools.product([station1, station2], [source1, source2]):
#         if PMF_profile.loc[station, source].isnull().all():
#             print("There is no {} in {}".format(source, station))
#             return
#
#     newFig = False
#     if not ax:
#         f = plt.figure()
#         newFig = True
#         ax = plt.gca()
#
#     SID = compute_SID(
#         PMF_profile.loc[station1, :], PMF_profile.loc[station2, :],
#         source1, source2,
#         isRelativeMass=isRelativeMass
#     )
#     PD = compute_PD(
#         PMF_profile.loc[station1, :], PMF_profile.loc[station2, :],
#         source1, source2,
#         isRelativeMass=isRelativeMass
#     )
#     ax.plot(SID, PD, "o", **plot_kw)
#     
#     if newFig:
#         plot_deltatool_pretty(ax=ax)
#
# def plot_similarityplot(PMF_profile, station1, station2, source1, source2=None,
#                         isRelativeMass=False, ax=None, plot_kw={}):
#     """
#     Plot the distance in the SID/PD space of 2 profiles for 2 stations.
#     """
#     if not source2:
#         source2 = source1
#
#     for station, source in itertools.product([station1, station2], [source1, source2]):
#         if PMF_profile.loc[station, source].isnull().all():
#             print("There is no {} in {}".format(source, station))
#             return
#
#     newFig = False
#     if not ax:
#         f = plt.figure()
#         newFig = True
#         ax = plt.gca()
#
#     SID = compute_SID(
#         PMF_profile.loc[station1, :], PMF_profile.loc[station2, :],
#         source1, source2,
#         isRelativeMass=isRelativeMass
#     )
#     PD = compute_PD(
#         PMF_profile.loc[station1, :], PMF_profile.loc[station2, :],
#         source1, source2,
#         isRelativeMass=isRelativeMass
#     )
#     ax.plot(SID, PD, "o", **plot_kw)
#     
#     if newFig:
#         plot_deltatool_pretty(ax=ax)

# def plot_relativeMass(PMF_profile, source="Biomass burning",
#                       isRelativeMass=True, totalVar="PM10", naxe=1,
#                       site_typologie=None):
#     if not site_typologie:
#         site_typologie = collections.OrderedDict()
#         site_typologie["Urban"] = ["TAL", "LY", "POI", "NIC", "MRS",
#                                    "PdB", "PROV", "NGT",
#                                    "LEN"]
#         site_typologie["Valley"] = ["CHAM", "GRE"]
#         site_typologie["Traffic"] = ["RBX", "STRAS"]
#         site_typologie["Rural"] = ["REV"]
#
#
#     carboneous = ["OC*", "EC"]
#     ions = ["Cl-", "NO3-", "SO42-", "Na+", "NH4+", "K+", "Mg2+", "Ca2+"]
#     organics = [
#         "MSA", "Polyols", "Levoglucosan", "Mannosan",
#     ]
#     metals = [
#         "Al", "As", "Ba", "Cd", "Co", "Cr", "Cs", "Cu", "Fe", "La", "Mn",
#         "Mo", "Ni", "Pb", "Rb", "Sb", "Se", "Sn", "Sr", "Ti", "V", "Zn"
#     ]
#     keep_index = ["PM10"] + carboneous + ions + organics + metals
#     if naxe==2:
#         keep_index1 = ['PM10'] + carboneous + ["NO3-", "SO42-", "NH4+", "Na+", "K+", "Ca2+", "Cl-",
#                                 "Polyols", "Fe", "Al", "Levoglucosan", "Mannosan"]
#         keep_index2 = ["Mg2+", "MSA"] 
#         keep_index2tmp = list(set(keep_index) - set(keep_index1) - set(["PM10", "Mg2+", "MSA"]))
#         keep_index2tmp.sort()
#         keep_index2 += keep_index2tmp
#     dfperµg = pd.DataFrame(columns=keep_index)
#     for station, df in PMF_profile.reset_index().groupby("station"):
#         df = df.set_index("specie").drop("station", axis=1)
#         if isRelativeMass:
#             dfperµg.loc[station, :] = df.loc[:, source]
#         else:
#             dfperµg.loc[station, :] = to_relativeMass(df.loc[:, source],
#                                                        totalVar=totalVar).T
#     dfperµg = dfperµg.convert_objects(convert_numeric=True)
#
#     # FIGURE
#     f, axes = plt.subplots(nrows=naxe, ncols=1, figsize=(15,10))
#     if naxe == 1:
#         axes = [axes, None]
#
#     d = dfperµg.T.copy()
#     d["specie"] = d.index
#     # for i, keep_index in enumerate([keep_index1, keep_index2]):
#     if naxe == 1:
#         xtick_list = [keep_index]
#     elif naxe == 2:
#         xtick_list = [keep_index1, keep_index2]
#     for i, keep_index in enumerate(xtick_list):
#         dd = d.reindex(keep_index)
#         # dd.rename(rename_station, inplace=True, axis="columns")
#         dd = dd.melt(id_vars=["specie"])
#         # if not percent:
#         dd.replace({0: pd.np.nan}, inplace=True)
#         axes[i].set_yscale("log")
#         sns.boxplot(data=dd, x="specie", y="value", ax=axes[i], color="white",
#                     showcaps=False,
#                     showmeans=False, meanprops={"marker": "d"})
#         ntypo = len(site_typologie.keys())
#         colors = list(TABLEAU_COLORS.values())
#         # for sp, specie in enumerate(keep_index):
#         for t, typo in enumerate(site_typologie.keys()):
#             if typo == "Urban":
#                 marker = "*"
#             elif (typo == "Valley") or typo == ("Urban+Alps"):
#                 marker = "o"
#             elif typo == "Traffic":
#                 marker = "p"
#             else:
#                 marker = "d"
#             step = 0.1
#             for j, site in enumerate(site_typologie[typo]):
#                 if site not in PMF_profile.index.get_level_values("station").unique():
#                     continue
#                 axes[i].scatter(
#                     pd.np.arange(0,len(keep_index))-ntypo*step/2+step/2+t*step,
#                     d.loc[keep_index, site],
#                     marker=marker, color=colors[j], alpha=0.8
#                 )
#
#         # sns.swarmplot(data=dd, x="specie", y="value", color=".2", alpha=0.5, size=4,
#         #              ax=axes[i]) 
#     if naxe == 1:
#         axes[0].set_ylim([1e-5,2])
#     else:
#         axes[0].set_ylim([1e-3,1])
#         axes[1].set_ylim([1e-5,1e-2])
#     for ax in axes:
#         if ax:
#             ax.set_ylabel("µg/µg of PM$_{10}$")
#             ax.set_xlabel("")
#             for tick in ax.get_xticklabels():
#                 tick.set_rotation(90)
#             # ax.legend(loc="center", 
#             #           ncol=(len(dfperµg.columns))//2,
#             #           bbox_to_anchor=(0.5, -.55),
#             #           frameon=False)
#     # create custom legend
#     labels = []
#     artists = []
#     for typo in site_typologie.keys():
#         if typo == "Urban":
#             marker = "*"
#         elif (typo == "Valley") or (typo == "Urban+Alps"):
#             marker = "o"
#         elif typo == "Traffic":
#             marker = "p"
#         else:
#             marker = "d"
#         noSiteYet = True
#         for j, site in enumerate(site_typologie[typo]):
#             if site not in PMF_profile.index.get_level_values("station").unique():
#                 continue
#             if noSiteYet:
#                 artist = typo
#                 label = ""
#                 artists.append(artist)
#                 labels.append(label)
#                 noSiteYet = False
#             artist = mlines.Line2D([], [], ls='', marker=marker, color=colors[j],
#                                    label=site)
#             artists.append(artist)
#             labels.append(site)
#
#     axes[0].legend(artists, labels, bbox_to_anchor=(1,1),
#                    handler_map={str: LegendTitle()},
#                    frameon=False
#               )
#     # ax.legend('', frameon=False)
#
#     f.suptitle(source)
#     plt.subplots_adjust(left=0.07, right=0.83, bottom=0.15, top=0.900, hspace=0.5)
