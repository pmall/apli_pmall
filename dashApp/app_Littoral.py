# -*- coding: utf-8 -*-
import sqlite3
import pandas as pd
import numpy as np
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
import plotly.graph_objs as go
import plotly.express as px

from django.conf import settings
# from .server import app

from django_plotly_dash import DjangoDash

import dashApp.utilities as utilities
from .app_components import SharedComponent
from .callbacks import register_callbacks

sc = SharedComponent()

STATION_DEMO = ["Le Havre", "Dieppe-Ailly", "Arrest", "Calais"]

# =============================================================================
conn = sqlite3.connect(settings.BDDPM) # BDDPM must be defined in local_settings.py
df = pd.read_sql(
        "SELECT Date FROM `values_all` WHERE Station IN ('{}');".format("', '".join(STATION_DEMO)),
        conn, parse_dates=["Date"]
        )
conn.close()
# =============================================================================

# ==== STATION LIST ===========================================================
list_station = STATION_DEMO
list_station_OP = STATION_DEMO
# ==== END STATION LIST =======================================================


external_css = [
    "/static/css/apps.css",
]

app = DjangoDash("app_Littoral",
                 external_stylesheets=external_css,
                )

app.list_station = list_station
app.list_station_OP = list_station_OP


app.layout = sc.get_layout(list_station=list_station, df=df)


external_css = [
    app.get_asset_url("apps.css"), # litle hack to serve django css assets
]

register_callbacks(app)

for css in external_css:
    app.css.append_css({"external_url": css})


if __name__ == '__main__':
    app = _create_app()
    app.run_server(**server_setting)
