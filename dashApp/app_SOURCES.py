# -*- coding: utf-8 -*-
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import pandas as pd
import sqlite3
import copy

from django.conf import settings
from django_plotly_dash import DjangoDash

import dashApp.utilities as utilities
from .app_components import SharedComponent, SOURCESComponents
# from .deltatool import *

sc = SharedComponent()
sourcesc = SOURCESComponents()

# BDD connexion ===============================================================

# DBPATH = "./DB_SOURCES.db"
conn = sqlite3.connect(settings.BDDSOURCES)

SID = pd.read_sql(
    "SELECT * FROM SID;",
    con=conn,
)

PD = pd.read_sql(
    "SELECT * FROM PD;",
    con=conn,
)
conn.close()
# =============================================================================
SID.set_index(["Source", "Station"], inplace=True)
PD.set_index(["Source", "Station"], inplace=True)
if "index" in SID.columns:
    SID.drop("index", axis=1, inplace=True)
if "index" in PD.columns:
    PD.drop("index", axis=1, inplace=True)


STATIONS = [
    'POI', 'LEN', 'MRS', 'PROV', 'CHAM', 'NGT', 'REV', 'ROU', 'NIC', 'PdB',
    'TAL', 'RBX', 'STRAS', 'LY', 'GRE'
]

BASE_VAR_SP = ["Date", "Station"]
BASE_VAR_SRC = ["Date", "Station"]

SELECTEDSTATION = set()

carboneous = ["OC*", "EC"]
ions = ["Cl-", "NO3-", "SO42-", "Na+", "NH4+", "K+", "Mg2+", "Ca2+"]
organics = [
    "MSA", "Polyols", "Levoglucosan", "Mannosan",
]
metals = [
    "Al", "As", "Ba", "Cd", "Co", "Cr", "Cs", "Cu", "Fe", "La", "Mn",
    "Mo", "Ni", "Pb", "Rb", "Sb", "Se", "Sn", "Sr", "Ti", "V", "Zn"
]
SPECIES_ORDER = ["PM10"] + carboneous + ions + organics + metals

# do not add species to plot when we already have too many...
tooManyPlot = 30
minSample = 40

app = DjangoDash("app_SOURCES")
# server = app.server

app.layout = html.Div(
    id="main",
    children=[
        # first column
        html.Div(
            id="first-column",
            children=[
                html.Div(children=[
                    html.Div(
                        [
                            sc.dropdown_component(
                                label="Station",
                                id="station-dropdown",
                                options=[{"label": l, "value": l} for l in
                                         STATIONS],
                                default=STATIONS
                            ),
                            sc.dropdown_component(
                                label="PMF factor",
                                id="source-dropdown",
                                default=[]
                            ),
                            sc.dropdown_component(
                                label="Specie",
                                id="specie-dropdown",
                                default=[]
                            ),
                        ]
                    ),
                    sourcesc.get_map_component(STATIONS),

                    html.Br(),

                    html.Div([
                        sourcesc.gettinghelp_component
                    ])
                ]),
            ],
        ),  # end first col
        # second column
        html.Div(
            id="second-column",
            children=[
                html.Div(children=[
                    dcc.Tabs(
                        id="tab_tsordt",
                        value=1,
                        children=[
                            dcc.Tab(
                                label="TimeSerie", value=1,
                                children=[sc.timeserie_component]
                            ),
                            dcc.Tab(
                                label="Profile", value=2,
                                children=[sourcesc.profile_component]
                            ),
                            dcc.Tab(
                                label="DeltaTool", value=3,
                                children=[sourcesc.deltatool_component]
                            ),
                            dcc.Tab(
                                label="Uncertainties", value=4,
                                children=[
                                    # specie_dropdown_component,
                                    sourcesc.uncertainty_component
                                ]
                            ),
                            dcc.Tab(
                                label="Species repartition", value=5,
                                children=[
                                    sourcesc.specie_repartition_component
                                ]
                            )
                        ],
                    ),
                ]),
            ],
        )  # end second col
    ],
)
# end main

external_css = [
    "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css",
    "/static/css/apps.css",  # litle hack to serve django css assets
]

for css in external_css:
        app.css.append_css({"external_url": css})


@app.callback(Output('specie-dropdown', 'options'),
              [Input('station-dropdown', 'value')])
def set_specie_option(stations):
    """Set the possible specie dropdown"""
    conn = sqlite3.connect(settings.BDDSOURCES)
    query = "SELECT * FROM profiles_constrained WHERE station IN ('{stations}')".format(
        stations="', '".join(stations)
    )
    species = pd.read_sql(query, con=conn)["Specie"].unique()
    conn.close()
    # species = profile.loc[profile["Station"].isin(stations), "Specie"].unique()
    species = set(species) - set(BASE_VAR_SP)
    species = list(species)
    # specie to plot
    carboneous = ["OC*", "EC"]
    ions = ["Cl-", "NO3-", "SO42-", "Na+", "NH4+", "K+", "Mg2+", "Ca2+"]
    organics = [
        "MSA", "Polyols", "Levoglucosan", "Mannosan",
    ]
    metals = [
        "Al", "As", "Ba", "Cd", "Co", "Cr", "Cs", "Cu", "Fe", "La", "Mn",
        "Mo", "Ni", "Pb", "Rb", "Sb", "Se", "Sn", "Sr", "Ti", "V", "Zn"
    ]
    species_order = ["PM10"] + carboneous + ions + organics + metals
    for sp in species_order:
        if sp not in species:
            species_order.remove(sp)
    return [{'label': i, 'value': i} for i in species_order]


@app.callback(Output('source-dropdown', 'options'),
              [Input('station-dropdown', 'value')])
def set_source_option(stations_dd):
    """Set the possible source dropdown"""
    conn = sqlite3.connect(settings.BDDSOURCES)
    query = "SELECT * FROM profiles_constrained WHERE station IN ('{stations}')".format(
        stations="', '".join(stations_dd)
    )
    sources = pd.read_sql(query, con=conn).dropna(axis=1, how='all').columns
    conn.close()
    # sources = []
    # sources = profile.loc[profile["Station"].isin(stations_dd)].dropna(axis=1, how='all').columns
    sources = set(sources) - set(BASE_VAR_SRC)
    sources = list(sources)
    if "Specie" in sources:
        sources.remove("Specie")
    sources.sort()

    return [{'label': i, 'value': i} for i in sources]


def update_selected_station(map4station, dropdown4station):
    """
    Update the SELECTEDSTATION variable according to both the map and the
    dropwdown list of station.
    """
    #TODO: lasso selection
    # if values1:
    #     for point in values1["points"]:
    #         s = point["text"]
    #         print(s)
    #         if s in SELECTEDSTATIONMAP:
    #             SELECTEDSTATIONMAP.remove(s)
    #         else:
    #             SELECTEDSTATIONMAP.append(s)

    # reset selected station and repopulate it
    if not map4station:
        SELECTEDSTATION.clear()
    # point selected on the map
    if map4station:
        for point in map4station["points"]:
            s = point["text"]
            if s in SELECTEDSTATION:
                SELECTEDSTATION.remove(s)
                dropdown4station.remove(s)
            else:
                SELECTEDSTATION.update([s])
    # add also station mannualy selected
    SELECTEDSTATION.update(dropdown4station)

    # no return. SELECTEDSTATION is a static variable


@app.callback(Output('station-dropdown', 'value'),
              [Input('map-graph', 'clickData')],
              [State('station-dropdown', 'value')])
def update_dropdown_station_selected(values, stations):
    update_selected_station(values, stations)
    return list(SELECTEDSTATION)


@app.callback(Output('map-graph', 'figure'),
              [Input('station-dropdown', 'value')],
              [State('map-graph', 'figure')])
def update_map_station_selected(stations, figure):
    update_selected_station(None, stations)
    # TODO: update_selected_station is called even if the stations variable is
    # unchanged.

    data = sourcesc.get_map_data(STATIONS, SELECTEDSTATION)

    # query = "SELECT * FROM metadata_station WHERE abbrv in ('{stations}')".format(
    #     stations="', '".join(SELECTEDSTATION)
    # )
    # conn = sqlite3.connect(settings.BDDSOURCES)
    # dfmap = pd.read_sql(query, con=conn)
    # conn.close()
    # markers_typo = {
    #     "urban": "square",
    #     "periurban": "circle",
    #     "rural": "diamond"
    # }
    # for typo in dfmap["area type"].unique():
    #     idx = (dfmap["area type"] == typo) & (dfmap["abbrv"].isin(SELECTEDSTATION))
    #     data.append(
    #         dict(
    #             type= 'scattergeo',
    #             lon=dfmap.loc[idx, 'longitude'],
    #             lat=dfmap.loc[idx, 'latitude'],
    #             text=dfmap.loc[idx, 'abbrv'],
    #             mode = 'markers',
    #             name = typo,
    #             marker = dict(
    #                 symbol=markers_typo[typo],
    #                 size=10,
    #                 color="orange"
    #             ),
    #             showlegend=False
    #         )
    #     )
    figure = {"data": data, "layout": sourcesc.get_map_layout()}
    return figure


@app.callback(Output('ts-graph', 'figure'),
              [Input('station-dropdown', 'value'),
              Input('specie-dropdown', 'value'),
              Input('source-dropdown', 'value'),
              Input('tab_tsordt', 'value')])
def update_ts_graph(stations, species, sources, tabselected):
    """Plot a time serie of the concentration for the selected station and
    specie/source
    """
    traces=[]
    returnError = {
            'data': traces,
            'layout': {'title': 'PM concentration'}
            }

    if tabselected != 1:
        return returnError

    # remove ability to plot species
    species = []
    if (len(species) + len(sources)) == 0:
        print("TS: len(species+sources)==0")
        return returnError

    nbPlot = len(stations) * (len(sources))
    if nbPlot > tooManyPlot:
        errorMsg = (
            "Too many variable to plot (>{toMuch}). Try reduce either the number of "
            "station or source factor"
        ).format(toMuch=tooManyPlot)
        returnError["layout"]["title"] = errorMsg
        return returnError

    dfdt = utilities.get_contribution(sources, species, stations)

    if len(dfdt.columns)==2:
        print("TS: Only date and station")
        return returnError
    dfdt["labels"] = pd.np.nan
    stations = dfdt["Station"].unique()
    # species = dfdt.columns

    dfdt["labels"].fillna(value=pd.np.nan, inplace=True)

    # should never happen... but just in case
    if "Date" not in dfdt.columns:
        print("TS: no date given")
        return returnError

    species = set(species) - set(BASE_VAR_SP)
    sources = set(sources) - set(BASE_VAR_SRC)

    for station in stations:
        dftmp = dfdt[dfdt["Station"] == station]
        for toplot, var in zip(("species", "sources"), (species, sources)):
            if len(var) == 0:
                continue
            if pd.isnull(dftmp[list(var)]).all().all():
                continue
            groupby = []
            if toplot == "species":
                if any(dftmp["labels"].notnull()):
                    groupby += ["labels"]
            elif toplot == "sources":
                groupby += []  #"programme PMF"]
            traces += [i for i in utilities.plot_ts(dftmp, station, var, groupby)]

    to_return = {
        'data': traces,
        'layout': go.Layout(
            title="PM concentration",
            yaxis={"title": 'µg/m3'},
            showlegend=True,
            margin=go.layout.Margin(
                l=50,
                r=00,
                b=50,
                t=50,
                pad=0
            ),
            legend=dict(orientation="h")
        )
    }

    return to_return


@app.callback(Output('box-graph', 'figure'),
              [Input('tab-boxplot', 'value'),
              Input('boxplot-options-graph-type', 'value'),
              Input('boxplot-options-groupby', 'value'),
              Input('specie-dropdown', 'value'),
              Input('source-dropdown', 'value'),
              Input('station-dropdown', 'value'),
              Input('tab_tsordt', 'value')])
def update_box_graph(temporality, plots_options, groupby_var, species,
                     sources, stations, tabselected):
    """Seasonal boxplot or barplot"""
    traces = []
    returnError = {
            'data': traces,
            'layout': {'title': 'Seasonal dispersion'}
            }
    if tabselected != 1:
        return returnError

    if (len(species) + len(sources)) == 0:
        return returnError

    dfdt = utilities.get_contribution(sources, species, stations)

    stations = dfdt["Station"].unique()

    species = set(species) - set(BASE_VAR_SP)
    sources = set(sources) - set(BASE_VAR_SRC)

    nbPlot = len(stations) * len(sources)
    if nbPlot > tooManyPlot:
        errorMsg = (
            "Too many variable to plot (>{toMuch}). Try reduce either the "
            "number of station or source factor"
        ).format(toMuch=tooManyPlot)
        returnError["layout"]["title"] = errorMsg
        return returnError

    # ==== Plot option ========================================================
    plot_type = "box" if "boxplot" in plots_options else "bar"
    # X variable
    station_order = [
        "REV", "MRS", "PdB", "PROV", "NIC", "TAL", "POI", "LEN", "NGT", "ROU",
        "LY", "GRE", "CHAM", "RBX", "STRAS"
    ]
    stations = [s for s in station_order if s in stations]
    if temporality == 1:
        date_var_list = ["DJF", "MAM", "JJA", "SON"]
    elif temporality == 2:
        date_var_list = [
            "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep",
            "Oct", "Nov", "Dec"
        ]
    x_var = groupby_var
    if x_var == "Date":
        x_var_col = "season" if temporality == 1 else "month"
        x_var_list = date_var_list
        hue_var_col = "Station"
        hue_var_list = stations
    elif x_var == "Site":
        x_var_col = "Station"
        x_var_list = stations
        hue_var_col = "season" if temporality == 1 else "month"
        hue_var_list = date_var_list
    xticklabels = x_var_list

    dfdt = utilities.add_month(dfdt, season=True)

    # ==== Plot part ==========================================================
    for hue in hue_var_list:
        dftmp = dfdt[dfdt[hue_var_col] == hue]
        for toplot, y_var in zip(("species", "sources"), (species, sources)):
            if len(y_var) == 0 or pd.isnull(dftmp[list(y_var)]).all().all():
                continue
            groupby = []
            traces += [
                i for i in utilities.plot_box(
                    dftmp,
                    hue,
                    x_var=x_var_col,
                    y_var=y_var,
                    groupby=groupby,
                    plot_type=plot_type
                )
            ]

    return {
        'data': traces,
        'layout': go.Layout(
            yaxis={"title": "µg/m3"},
            xaxis={'categoryorder': 'array',
                   'categoryarray': xticklabels},
            showlegend=True,
            boxmode='group',
            margin=go.layout.Margin(
                l=50,
                r=00,
                b=50,
                t=50,
                pad=0
            ),
            legend=dict(orientation="h")
        )
    }


@app.callback(Output('totalspeciesum-graph', 'figure'),
              [Input('source-dropdown', 'value'),
               Input('station-dropdown', 'value'),
               Input('tab_tsordt', 'value')])
def update_totalspeciesum_graph(sources, stations, tabselected):
    """Update the profile tab, concentration of specie/sum(specie)
    """
    data = []
    to_return = {
            'data': data,
            "layout": {"title": "Contribution to total specie sum"}
        }

    if tabselected != 2:
        return to_return

    if len(stations) == 0:
        return to_return

    query = "SELECT * FROM profiles_constrained WHERE station IN ('{stations}');".format(
        stations="', '".join(stations)
    )
    conn = sqlite3.connect(settings.BDDSOURCES)
    d = pd.read_sql(query, con=conn, index_col=["Station"])\
            .drop("index", axis=1)
    conn.close()
    for s in sources:
        data.append(go.Box(
            x=d["Specie"],
            y=d[s]/d.sum(axis=1).values * 100,
            name=s)
        )
    # specie to plot
    carboneous = ["OC*", "EC"]
    ions = ["Cl-", "NO3-", "SO42-", "Na+", "NH4+", "K+", "Mg2+", "Ca2+"]
    organics = [
        "MSA", "Polyols", "Levoglucosan", "Mannosan",
    ]
    metals = [
        "Al", "As", "Ba", "Cd", "Co", "Cr", "Cs", "Cu", "Fe", "La", "Mn",
        "Mo", "Ni", "Pb", "Rb", "Sb", "Se", "Sn", "Sr", "Ti", "V", "Zn"
    ]
    keep_index = ["PM10"] + carboneous + ions + organics + metals

    to_return = {
        'data': data,
        'layout': go.Layout(
            yaxis={
                "title": "% of total specie sum",
                "range": [0, 100]
                  },
            xaxis={'categoryorder': 'array',
                   'categoryarray': keep_index},
            showlegend=True,
            boxmode='group',
            margin=go.layout.Margin(
                l=50,
                r=00,
                b=50,
                t=50,
                pad=0
            ),
            legend=dict(orientation="h")
        )
    }

    return to_return


@app.callback(Output('concentration-graph', 'figure'),
              [Input('source-dropdown', 'value'),
               Input('station-dropdown', 'value'),
               Input('tab_tsordt', 'value')])
def update_concentration_graph(sources, stations, tabselected):
    """Update the profile tab: concentration un g/g
    """
    data = []
    to_return = {
        "data": [],
        "layout": {"title": "Contribution to total specie sum"}
            }

    if tabselected != 2:
        return to_return

    if len(stations) == 0:
        return to_return

    query = "SELECT \"{sources}\" FROM profiles_constrained WHERE station IN ('{stations}');".format(
        sources='", "'.join(sources+["Station", "Specie"]),
        stations="', '".join(stations),
    )
    conn = sqlite3.connect(settings.BDDSOURCES)
    d = pd.read_sql(query, con=conn, index_col=["Station", "Specie"])
    conn.close()
    # d = profile.set_index(["Station", "Specie"])
    # d = d.loc[stations]
    d = d / d.xs("PM10", level="Specie")
    # loc[(slice(None), "PM10"), :]\
    #         .reset_index(level="Specie", drop=True)
    # \
    #         .set_index("Station")\
    #         .drop("Specie", axis=1)
    d = d.reset_index()
    d.replace({0: pd.np.nan}, inplace=True)
    for s in sources:
        data.append(go.Box(
            x=d["Specie"],
            y=d.loc[d["Station"].isin(stations), s],
            name=s)
        )
    # specie to plot
    carboneous = ["OC*", "EC"]
    ions = ["Cl-", "NO3-", "SO42-", "Na+", "NH4+", "K+", "Mg2+", "Ca2+"]
    organics = [
        "MSA", "Polyols", "Levoglucosan", "Mannosan",
    ]
    metals = [
        "Al", "As", "Ba", "Cd", "Co", "Cr", "Cs", "Cu", "Fe", "La", "Mn",
        "Mo", "Ni", "Pb", "Rb", "Sb", "Se", "Sn", "Sr", "Ti", "V", "Zn"
    ]
    keep_index = ["PM10"] + carboneous + ions + organics + metals

    to_return = {
        'data': data,
        'layout': go.Layout(
            yaxis={"title": "g/g of PM10",
                   "type": "log"},
            xaxis={'categoryorder': 'array',
                   'categoryarray': keep_index},
            showlegend=True,
            boxmode='group',
            margin=go.layout.Margin(
                l=50,
                r=00,
                b=50,
                t=50,
                pad=0
            ),
            legend=dict(orientation="h")
        )
    }

    return to_return


@app.callback(Output('uncertainty-graph-conc', 'figure'),
              [Input('source-dropdown', 'value'),
               Input('specie-dropdown', 'value'),
               Input('station-dropdown', 'value'),
               Input('tab_tsordt', 'value')])
def update_uncertainty_graph_conc(sources, species, stations, tabselected):

    """Plot the BS and DISP error for the sources/species and the stations.

    :sources: TODO
    :species: TODO
    :stations: TODO
    :returns: TODO

    """
    data = []
    to_return = {
            "data": data,
            "layout": {"title": "Contribution µg/m3"}
        }

    if tabselected != 4:
        return to_return

    if len(stations) == 0:
        to_return["layout"]["title"] = "Select at least 1 station"
        return to_return
    if len(sources) == 0:
        to_return["layout"]["title"] = "Select at least 1 profile"
        return to_return
    if len(species) == 0:
        to_return["layout"]["title"] = "Select at least 1 specie"
        return to_return

    traces = []
    # ==== DB get
    queryUnc = 'SELECT * \
            FROM uncertainties_summary_constrained \
            WHERE station IN ("{station}") \
            AND specie IN ("{sp}") \
            AND profile IN ("{p}")\
            ;'.format(
            sp='", "'.join(species),
            station='", "'.join(stations),
            p='", "'.join(sources)
            )
    queryBS = 'SELECT * FROM BS_profiles_constrained WHERE\
            station IN ("{station}") AND\
            specie IN ("{sp}") AND\
            profile IN ("{p}")\
            ;'.format(
            sp='", "'.join(species),
            station='", "'.join(stations),
            p='", "'.join(sources)
            )
    conn = sqlite3.connect(settings.BDDSOURCES)
    dfUnc = pd.read_sql(queryUnc, con=conn, index_col=["Station", "Profile", "Specie"]).drop("index", axis=1)
    dfBS = pd.read_sql(queryBS, con=conn, index_col=["Station", "Profile", "Specie"]).drop("index", axis=1)
    conn.close()
    # ==== Base run
    for source in sources:
        for sp in species:
            dftmpRef = dfUnc["Constrained base run"].xs((source, sp), level=["Profile", "Specie"])
            dftmpRef = dftmpRef.reset_index().melt(id_vars=["Station"])
            traces.append(
                go.Box(
                    x=dftmpRef.loc[:, "Station"],
                    y=dftmpRef.loc[:, "value"],
                    name="Base run-{}".format(source+"-"+sp)
                )
            )
            # ==== BS
            dftmpBS = dfBS.xs((source, sp), level=["Profile", "Specie"])
            dftmpBS = dftmpBS.reset_index().melt(id_vars=["Station"])
            traces.append(
                go.Box(
                    x=dftmpBS.loc[:, "Station"],
                    y=dftmpBS.loc[:, "value"],
                    name="BS-{}".format(source+"-"+sp)
                )
            )
            # ==== DISP
            dftmpDisp = dfUnc[["DISP Min", "DISP average", "DISP Max"]].xs((source, sp), level=["Profile", "Specie"])
            dftmpDisp = dftmpDisp.reset_index().melt(id_vars=["Station"])
            traces.append(
                go.Box(
                    x=dftmpDisp.loc[:, "Station"],
                    y=dftmpDisp.loc[:, "value"],
                    name="DISP-{}".format(source+"-"+sp)
                )
            )


    x_order = ["REV", "MRS", "PdB", "PROV", "NIC", "TAL", "POI", "LEN", "NGT",
            "ROU", "LY", "GRE", "CHAM", "RBX", "STRAS"]

    layout = dict(
            title="Contribution µg/m3",
            yaxis={"title": "µg/m3"},
            xaxis={'categoryorder': 'array',
                   'categoryarray': x_order},
            boxmode='group',
            legend=dict(orientation="h")
            )

    to_return["data"] = traces
    to_return["layout"] = layout
    return to_return


@app.callback(Output('uncertainty-graph-norm', 'figure'),
              [Input('source-dropdown', 'value'),
               Input('specie-dropdown', 'value'),
               Input('station-dropdown', 'value'),
               Input('tab_tsordt', 'value')])
def update_uncertainty_graph_norm(sources, species, stations, tabselected):
    """Plot the BS and DISP error for the sources/species and the stations.

    :sources: TODO
    :species: TODO
    :stations: TODO
    :returns: TODO

    """
    data = []
    to_return = {
            "data": data,
            "layout": {"title": "Contribution to µg/µg"}
        }

    if tabselected != 4:
        return to_return

    if len(stations) == 0:
        to_return["layout"]["title"] = "Select at least 1 station"
        return to_return
    if len(sources) == 0:
        to_return["layout"]["title"] = "Select at least 1 profile"
        return to_return
    if len(species) == 0:
        to_return["layout"]["title"] = "Select at least 1 specie"
        return to_return


    traces = []
    # ==== DB get
    addPM10 = False
    if "PM10" not in species:
        species.append("PM10")
        addPM10 = True
    queryUnc = 'SELECT * FROM uncertainties_summary_constrained WHERE\
            station IN ("{station}") AND\
            specie IN ("{sp}") AND\
            profile IN ("{p}")\
            ;'.format(
            sp='", "'.join(species),
            station='", "'.join(stations),
            p='", "'.join(sources)
            )
    queryBS = 'SELECT * FROM BS_profiles_constrained WHERE\
            station IN ("{station}") AND\
            specie IN ("{sp}") AND\
            profile IN ("{p}")\
            ;'.format(
            sp='", "'.join(species),
            station='", "'.join(stations),
            p='", "'.join(sources)
            )
    conn = sqlite3.connect(settings.BDDSOURCES)
    dfBS = pd.read_sql(queryBS, con=conn, index_col=["Station", "Profile", "Specie"]).drop("index", axis=1)
    dfUnc = pd.read_sql(queryUnc, con=conn, index_col=["Station", "Profile", "Specie"]).drop("index", axis=1)
    conn.close()

    if addPM10:
        species.remove("PM10")
    for source in sources:
        dfnormUnc = dfUnc["Constrained base run"].xs((source, "PM10"), level=["Profile", "Specie"])
        dfnormBS = dfBS.xs((source, "PM10"), level=["Profile", "Specie"])
        for sp in species:
            # ==== Ref run
            dftmpRef = dfUnc["Constrained base run"].xs((source, sp), level=["Profile", "Specie"])
            dftmpRef = dftmpRef / dfnormUnc
            dftmpRef = dftmpRef.reset_index().melt(id_vars=["Station"])
            traces.append(
                go.Box(
                    x=dftmpRef.loc[:, "Station"],
                    y=dftmpRef.loc[:, "value"],
                    name="Base run-{}".format(source+"-"+sp)
                )
            )
            # ==== BS run
            dftmpBS = dfBS.xs((source, sp), level=["Profile", "Specie"])
            dftmpBS = dftmpBS / dfnormBS
            dftmpBS = dftmpBS.reset_index().melt(id_vars=["Station"])
            traces.append(
                go.Box(
                    x=dftmpBS.loc[:, "Station"],
                    y=dftmpBS.loc[:, "value"],
                    name="BS-{}".format(source+"-"+sp)
                )
            )
            # ==== DISP run
            # It is not possible to know which DISP run is associated to which results,
            # so there is no sens to present them.
            traces.append(
                go.Box(
                    x=dftmpBS.loc[:, "Station"],
                    y=[pd.np.nan],
                    name="No normalized DISP"
                )
            )

    x_order = ["REV", "MRS", "PdB", "PROV", "NIC", "TAL", "POI", "LEN", "NGT",
            "ROU", "LY", "GRE", "CHAM", "RBX", "STRAS"]

    layout = dict(
            title="Contribution to µg/µg",
            yaxis={
                "title": "µg/µg",
                "range": [0, 1],
                },
            xaxis={'categoryorder': 'array',
                   'categoryarray': x_order},
            boxmode='group',
            legend=dict(orientation="h")
            )

    to_return["data"] = traces
    to_return["layout"] = layout
    return to_return


@app.callback(Output('deltatool-graph', 'figure'),
              [Input('source-dropdown', 'value'),
               Input('station-dropdown', 'value'),
               Input('deltatool-options', 'value')
              ])
def update_deltatool_graph(sources, stations, graphOptions):
    traces = []
    to_return = {
        'data': traces,
        'layout': go.Layout(
            hovermode="closest",
            yaxis={
                "title": "PD",
                "range": [0, 1],
                "scaleratio": 1
            },
            xaxis={
                "title": "SID",
                "range": [0, 1.5],
                "scaleanchor": "y"
            },
            showlegend=True,
            margin=go.layout.Margin(
                l=50,
                r=00,
                b=50,
                t=50,
                pad=0
            ),
            legend=dict(orientation="v"),
            shapes=[
                {
                    'type': 'rect',
                    'x0': 0,
                    'y0': 0,
                    'x1': 1,
                    'y1': 0.4,
                    'line': {
                        'width': 0,
                        },
                    'fillcolor': 'rgba(0, 255, 0, 0.1)',
                    },
                ]
        )
    }

    if len(sources)==0:
        return to_return 

    x = SID.loc[(sources, stations), stations].reset_index()\
            .melt(id_vars=["Source", "Station"])\
            .set_index(["Source", "Station"])\
            .loc[sources]  
    y = PD.loc[(sources, stations), stations].reset_index()\
            .melt(id_vars=["Source", "Station"])\
            .set_index(["Source", "Station"])\
            .loc[sources]  

    x = x[x["value"]>0].round(3)
    y = y[y["value"]>0].round(3)

    if graphOptions == "all":
        for source in sources:
            if len(x.xs(source, level="Source").dropna()["variable"].unique()) <= 1:
                continue
            text = x.xs(source, level="Source")["variable"] + "-" + x.loc[source].index.get_level_values("Station")
            traces.append(
                go.Scatter(
                    x=x.xs(source, level="Source")["value"],
                    y=y.xs(source, level="Source")["value"],
                    mode="markers",
                    marker=go.scatter.Marker(
                        color=utilities.get_sourceColor(source),
                        size=14,
                        line=dict(
                            color='#aaaaaa',
                            width=2
                        )
                    ),
                    hovertext=text,
                    name=source+" all",
                    showlegend=True,
                    opacity=0.5,
                    legendgroup='all',
                )
            )
    for source in sources:  # get a second loop for the Z order of the plots
        if len(x.xs(source, level="Source").dropna()["variable"].unique()) <= 1:
            continue
        traces.append(
            go.Scatter(
                x=[x.xs(source, level="Source")["value"].mean().round(3)],
                y=[y.xs(source, level="Source")["value"].mean().round(3)],
                error_x=dict(
                    visible=True,
                    type="constant",
                    symmetric=True,
                    value=x.xs(source, level="Source")["value"].std().round(3)
                ),
                error_y=dict(
                    visible=True,
                    type="constant",
                    symmetric=True,
                    value=y.xs(source, level="Source")["value"].std().round(3)
                ),
                mode="markers",
                marker=go.scatter.Marker(
                    color=utilities.get_sourceColor(source),
                    size=18,
                    line=dict(
                        color='#aaaaaa',
                        width=2
                    )
                ),
                hovertext="Mean±std",
                name=source,
                legendgroup='meanstd',
            )
        )
    to_return["data"] = traces

    return to_return

@app.callback(Output('specie-repartition-component', 'children'),
              [
               Input('station-dropdown', 'value'),
               Input('tab_tsordt', 'value')])
def update_specie_repartition_graph(stations, selectedTab):
    """Update the specie repartition among factor graph

    :species: TODO
    :returns: TODO

    """
    figure = {
        "data": [],
        "layout": {
            'title': 'Specie repartition among factors',
        }
    }

    if selectedTab != 5:
        return dcc.Graph(figure=figure)

    if len(stations) == 0:
        figure["layout"]["title"] = "Select at least 1 station"
        return figure
    # if len(sources) == 0:
    #     figure["layout"]["title"] = "Select at least 1 profile"
    #     return figure
    # if len(species) == 0:
    #     figure["layout"]["title"] = "Select at least 1 specie"
    #     return figure

    query = "SELECT * FROM profiles_constrained WHERE station IN ('{stations}');".format(
        stations="', '".join(stations)
    )
    conn = sqlite3.connect(settings.BDDSOURCES)
    d = pd.read_sql(query, con=conn, index_col=["Station", "Specie"])\
            .drop("index", axis=1)\
            .dropna(axis=1, how="all")
    conn.close()
    df = pd.DataFrame()

    sources = d.columns

    for s in sources:
        df[s] = d[s]/d.sum(axis=1).values * 100

    subplots = []
    for station in stations:
        dftmp = df.loc[station].dropna(axis=1, how="all")
        data = []
        for source in dftmp.columns:
            data.append(
                go.Bar(
                    x=dftmp.index,
                    y=dftmp[source].fillna(0),
                    name=source,
                    marker=dict(
                        color=utilities.get_sourceColor(source),
                    )
                )
            )

        figure["data"] = data
        figure["layout"] = go.Layout(
            title="Specie repartition among factor for {station}".format(station=station),
            yaxis={
                "title": "%",
            },
            xaxis={'categoryorder': 'array',
                   'categoryarray': SPECIES_ORDER},
            barmode='stack',
            legend=dict(x=0, y=1.15, orientation="h")
        )

        subplots.append(
            dcc.Graph(id="repartition-{station}".format(station=station),
                      figure=copy.deepcopy(figure))
        )
    return subplots

if __name__ == '__main__':
    app.run_server(debug=True)
