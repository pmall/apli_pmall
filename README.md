Data vizualisation for atmospheric PM
=====================================

This website/app aims to present and ease the exploration of atmospheric PM samples
database build at the IGE in the CHIANTI team (and collaborators), as well as
derived analysis such as PMF source-apportionment.

The data is the properties of several lab/institute. Do not re-use them without a
proper agreement. Hence, the actual database is not provided with this repository.

A demo can be found at: https://pmall.univ-grenoble-alpes.fr/demo.

Use case
========

Data vizualisation
------------------

Select the monitoring stations and variables (species, PM, etc.) in the dropdown
menu. You can also click on the map to (un)select the stations. 
Graphics will automatically update.


Download the data
-----------------

Due to ownership, the data is not available for everyone. Please contact us if
you want to work with this database.

The datatable summarize the selected variables.
To export them in a tabular format (namely, excel), click on `Export`.
If you want to filtre some values, click on the different `filter data` and write your
conditions.

For instance, let's download OC and EC of all the samples in Marnaz and Chamonix where the
Levoglucosan is higher than 500 ng⋅m⁻³, in 2014.

1. Select Chamonix and Marnaz in the `Station` dropdown
2. Select OC, EC, Levoglucosan in the `Species` dropdown
3. Move the date range slider between `2014-01` and `2015-01`.
4. Filtre the data: 
    * In the `Levoglucosan` ceil, write `>500`
5. Click on `Export`


![](assets/img/app_example.png)
