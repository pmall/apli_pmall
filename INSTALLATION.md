Installation
============

The app works with python 3.7. Here is detailled how to proceed with a debian
stretch. If you are on windows, please use anaconda.

    apt-get install python3-dev python3-venv

Clone the git depot:

    cd /your/favorite/path
    git clone https://gricad-gitlab.univ-grenoble-alpes.fr/pmall/apli_pmall

Create a virtual environment, activate it and install the dependancies:

    cd apli_pmall
    python3 -m venv venv
    source venv/bin/activate
    pip install -r requirements.txt

Configuration
=============

To configure the application, don't edit `pmall/settings.py` directly,
instead create a file `pmall/local_settings.py` with your local modifications.

Some Django things you should really configure:

 - `SECRET_KEY`
 - `ALLOWED_HOSTS`
 - `DEBUG`
 - database configuration

There are also pmall-specific settings:

 - `BDDPM`: path to the PM database for the dash app.
 - `INSTALLED_DASHAPP`: list of string, with the name of the apps located in
   `dashApp/app_{name}.py`.

Here is a example of a `local_settings.py`:

```py
import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = '&oupn^)a!at64fh3auyu+d#rfr)&z#cpdex5&o6^0m&866wec6y'

DEBUG = True

ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db_app.sqlite3'),
    }
}

BDDPM = os.path.join(BASE_DIR, "aerosols.db")

INSTALLED_DASHAPP = [
    "demo",
]

# List of App that do not require login
NO_LOGIN_APPS = [
    "demo"
]

SETTINGS_EXPORT = [
    "NO_LOGIN_APPS"
]
```

Then run the migrations:

    python3 manage.py migrate

And create a superuser:

    python3 manage.py createsuperuser

Lastly, you should collect static files to serve them:

    python3 manage.py collectstatic

Deployment
==========

Locally
-------

To launch the app, launch it as every django app:

    python3 manage.py runserver

then open a brower at `http://localhost:8000` and *tada*.

If you don't have a working database of PM -`BDDPM`, it will not work.
Ask Samuël Weber (samuel.weber [ at ] univ-grenoble-alpes.fr).

Production deployment with apache2
----------------------------------

### Install the app

To make the application widely accesible, we use the `www-data` user and the
root dir `/var/www/apli_pmall`.

    sudo su - www-data -s /bin/bash
    git clone https://gricad-gitlab.univ-grenoble-alpes.fr/pmall/apli_pmall
    cd apli_pmall

Then install the virtual environment and dependencies:

    python3 -m venv venv
    source venv/bin/activate
    pip install -r requirements.txt

### Nginx configuration

We need nginx and proxy for python3, so install it:

    sudo apt-get install nginx

Now, we need to configure nginx (add virtual host/etc).


    # Configuration du server
    server {
            listen 80; 
            server_name pmall.univ-grenoble-alpes.fr;
            return       301 https://pmall.univ-grenoble-alpes.fr$request_uri;
    }
    server {
        listen      443 ssl;
        server_name pmall.univ-grenoble-alpes.fr;
        charset     utf-8;
        ssl on;
        ssl_protocols TLSv1.2;

        ssl_certificate /etc/nginx/ssl/pmall_univ-grenoble-alpes_fr_all.cer;
        ssl_certificate_key /etc/nginx/ssl/pmall_univ-grenoble-alpes_fr.key;

        access_log /var/log/nginx/pmall.access.log;
        error_log /var/log/nginx/pmall.error.log;

        # Fichiers media et statiques, délivrés par nginx directement 
        location /media  {
            alias /var/www/apli_dash/media;
        }

        location /static {
            alias /var/www/apli_dash/static;
        }

        # Le reste va vers notre proxy uwsgi
        location / {
            include proxy_params;
            proxy_pass http://unix:/run/gunicorn.sock;
        }
    }

And finally configure gunicorn proxy and socket.
The systemd daemon in `/etc/systemd/system/gunicorn.service` :

    [Unit]
    Description=gunicorn daemon
    Requires=gunicorn.socket
    After=network.target

    [Service]
    Type=notify
    # the specific user that our service will run as
    User=www-data
    Group=www-data
    # another option for an even more restricted service is
    # DynamicUser=yes
    # see http://0pointer.net/blog/dynamic-users-with-systemd.html
    RuntimeDirectory=gunicorn
    WorkingDirectory=/var/www/apli_pmall/
    ExecStart=/var/www/apli_pmall/venv/bin/gunicorn wsgi
    ExecReload=/bin/kill -s HUP $MAINPID
    KillMode=mixed
    TimeoutStopSec=5
    PrivateTmp=true

    [Install]
    WantedBy=multi-user.target

The socket in `/etc/systemd/system/gunicorn.socket` :

    [Unit]
    Description=gunicorn socket

    [Socket]
    ListenStream=/run/gunicorn.sock
    # Our service won't need permissions for the socket, since it
    # inherits the file descriptor by socket activation
    # only the nginx daemon will need access to the socket
    SocketUser=www-data
    # Optionally restrict the socket permissions even more.
    # SocketMode=600

    [Install]
    WantedBy=sockets.target

