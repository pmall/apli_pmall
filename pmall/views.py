from django.shortcuts import render
from django.conf import settings


def home(request):
    return render(request, 'home.html', {'installedApp': settings.INSTALLED_DASHAPP})

def contact(request):
    return render(request, 'contact.html', {'installedApp': settings.INSTALLED_DASHAPP})

def apps(request):
    return render(request, 'dashApp.html', {'installedApp': settings.INSTALLED_DASHAPP})

