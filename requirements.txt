Django>=2.0,<3
django-email-obfuscator
django-bootstrap-static
django-settings-export==1.2.1
dpd-static-support
fontawesomefree==6.0.0b1
whitenoise
gunicorn

# Dash things
django-plotly-dash
dash==1.10
dash-renderer
dash-html-components
dash-core-components
dash-bootstrap-components
plotly>=3

# Computation
pandas
statsmodels
xlrd<2
openpyxl
