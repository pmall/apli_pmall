import sys
import os
import sqlite3
import numpy as np
import pandas as pd

XLSX_ENGINE = "xlrd"

MAP_BOOLEAN = {"FALSE": False, "TRUE": True}

class DataBase():
    """Create and store all the individual stations measurement.

    Parameters
    ----------

    filePM : str, BdD_PM_all_local.xlsx
        path to the database of filter analysis
    fileHF : str, BdD_haute-frequence.xlsx
        path to the high frequency database

    """
    def __init__(self, filePM=None, fileHF=None):
        """
        """
        self.stations = []

        if filePM is None:
            self.filePM = "BdD_PM_all_local.xlsx"
        else:
            self.filePM = filePM

        # if fileHF is None:
        #     self.fileHF = "BdD_HF_all_local.xlsx"
        # else:
        #     self.fileHF = fileHF

        self.naliste = ["NA", "n.a.", "n.a", "-", "na", "nan", "nd", "x", "/", "no data"]

        self.wbChem = self.get_wb_chem()
        self.dfChem = self.get_df_chem()
        # self.wbHF = self.get_wb_hf()
        # self.dfHF = self.get_df_hf()

        self.dateColumn = [
                "Date",
                "Sampling_started_at",
                "Sampling_ended_at",
                "Date_IC-sucres",
                "Date_metals"
                ]
        self.textColumn = [
                "Sample_ID",
                "Commentary",
                "Programme",
                "Ville",
                "Labels",
                "Date_exacte",
                "Particle_size"
                ]
        self.boolColumn = ["Big_serie", "Blank"]
        self.realColumn = list(
            set(list(self.dfChem.columns))
            - set(self.dateColumn+self.textColumn+self.boolColumn)
        )

        self.write_sql = False

        self.set_stations()

    def get_wb_chem(self):
        wbChem = pd.read_excel(self.filePM, sheet_name=None, header=0,
                               na_values=self.naliste,
                               engine=XLSX_ENGINE)
        return wbChem

    def get_wb_hf(self):
        wbhf = pd.read_excel(self.fileHF, sheet_name=None, header=0,
                engine=XLSX_ENGINE,
                na_values=self.naliste)
        return wbhf

    def get_df_chem(self):
        dfChem = pd.concat(self.wbChem, sort=False)

        dfChem.drop(["Template_serie"], inplace=True)

        self.capitalize_and_strip_columns(dfChem)

        self.merge_duplicated_columns(dfChem)

        dfChem.index.names = ["Station", "Number_ID"]
        dfChem.dropna(axis=1, how="all", inplace=True)

        return dfChem

    def get_df_hf(self):
        dfHF = pd.concat(self.wbHF, sort=False)

        self.capitalize_and_strip_columns(dfHF)
        self.merge_duplicated_columns(dfHF)

        dfHF.index.names = ["Station", "Number_ID"]
        dfHF.dropna(axis=1, how="all", inplace=True)

        return dfHF

    def set_stations(self):
        stationsChem = set(self.dfChem.index.get_level_values("Station"))
        # stationsHF = set(self.dfHF.index.get_level_values("Station"))
        stationsAll = set(stationsChem)

        # stationsAll.update(stationsHF)

        self.stationsChem = stationsChem
        # self.stationsHF = stationsHF
        self.stationsAll = stationsAll
        self.stationsOnlyChem = stationsAll #- set(stationsHF)
        self.stationsCommon = self.stationsAll - self.stationsOnlyChem

    def merge_duplicated_columns(self, df):
        """Merge 2 duplicated columns

        :df: TODO
        :returns: TODO

        """
        duplicated = set(df.columns[df.columns.duplicated()])
        if len(duplicated) == 0:
            return
        for dup in duplicated:
            try:
                dfduplicated = df[dup].copy()
                merged = dfduplicated.iloc[:, 0]
                for i in range(1, len(dfduplicated.columns)):
                    merged = merged.combine_first(dfduplicated.iloc[:, i])
                df.drop(dup, axis="columns", inplace=True)
                df[dup] = merged
            except Exception as e:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                raise ValueError("Error for column: {} (duplicated)\n{}\n{}".format(
                    dup,
                    dfduplicated.head(),
                    exc_value,
                    )
                    )

    def capitalize_and_strip_columns(self, df):
        """Rename the column with to their capitalized form
        syringic acid → Syringic_acid

        :df: pd.DataFrame

        """
        df.columns = [
            x[:1].upper() + x[1:]
            if ((type(x) == str) and (x[:1] != "δ")) else x
            for x in df.columns.str.strip().str.replace(" ", "_")
        ]

    def get_station(self, name):
        """Return the station with name `name`

        Parameters
        ----------

        name : str
            The name of the station

        Returns
        -------

        s : Station
            The station object requested
        """
        for s in self.stations:
            if s._name == name:
                return s

    def _build_stations(self):
        """Parse all stations and read data attached to them.

        It creates a `stations` list of all Station object.
        """
        for s in self.stationsAll:
            chem = None
            op = None
            hf = None
            if s in self.stationsChem:
                chem = self.dfChem.loc[s]
            # if s in self.stationsHF:
            #     hf = self.dfHF.loc[s]

            station = Station(name=s, raw_data_chem=chem, raw_data_hf=hf)
            station.build()
            
            self.stations.append(station)

    def create(self):
        """Build the database according to all stations in files

        Create df, df_header, df_units, df_QL and df_blanks dataframes.
        """
        self._build_stations()

        self.df = pd.concat({station._name: station.df for station in self.stations})
        self.df.index = self.df.index.droplevel()

        # self.df_hf = pd.concat({station._name: station.df_hf for station in self.stations})
        # self.df_hf.index = self.df_hf.index.droplevel()

        self.df_blanks = pd.concat({station._name: station.df_blanks for station in self.stations})
        self.df_blanks.index = self.df_blanks.index.droplevel()

        self.df_header = pd.concat(
                {station._name: station.df_header for station in self.stations},
                names=['Station', 'Number_ID']
                )
        self.df_header.index = self.df_header.index.droplevel()

        self.df_QL = self.df_header.loc[
            self.df_header["Sample_ID"].str.contains("QL").astype(bool)
        ].copy()
        self.df_QL = self.df_QL\
                .dropna(axis=1, how="all")\
                .infer_objects()\
                .reset_index()\
                .set_index(["Station", "Sample_ID", "Number_ID"])
        
        # ==== Units
        self.df_units = pd.concat(
                {station._name: station.df_units
                    for station in self.stations
                    if not station.df_units.empty
                    },
                names=['Station', 'Specie']
                )
        self.df_units = (
                self.df_units
                .dropna()
                .reset_index()
                .drop("Station", axis=1)
                )
        self.df_units = (
                self.df_units.loc[~self.df_units.duplicated()]
                .set_index("Specie")
                )
        self.df_units.columns = ["Unit"]

        # ==== Values
        self.df.dropna(axis=1, how="all", inplace=True)

        self.ensure_value_dtype(self.df)
        self.ensure_value_dtype(self.df_blanks)
        self.ensure_value_dtype(self.df_QL)


    def replace_DLandQL(self, df):
        """Replace inplace <QL and <DL value by error code

        `<QL` and `<LQ` are replaced by -1
        `<DL` and `<LD` are replaced by -2

        Parameters
        ----------

        df : pd.DataFrame

        """
        QLandDL = {
            "<QL": -1, "<LQ": -1,
            "<DL": -2, "<LD": -2,
        }
        df.replace(QLandDL, inplace=True)

    def ensure_value_dtype(self, df):
        """Convert each column to its known dtype
        First, try to infer it.

        Parameters
        ----------
        
        df : pd.DataFrame
        """
        # get columns
        dateColumn_ = set(df.columns).intersection(set(self.dateColumn))
        realColumn_ = set(df.columns).intersection(set(self.realColumn))
        textColumn_ = set(df.columns).intersection(set(self.textColumn))
        boolColumn_ = set(df.columns).intersection(set(self.boolColumn))

        # Let's pandas do its job first
        # df = df.infer_objects()

        # ensure date is a date
        for d in dateColumn_:
            if d in df.columns and df[d].dtypes != 'datetime64[ns]':
                try:
                    df.loc[:, d] = df[d].apply(pd.to_datetime)#pd.to_datetime(df[d])
                except Exception as e:
                    exc_type, exc_value, exc_traceback = sys.exc_info()
                    raise ValueError("Error in column {}\n{}".format(d, exc_value))

        # ensure bool is a bool
        if boolColumn_:
            df.loc[:,boolColumn_] = df.loc[:, boolColumn_].fillna(False).astype(bool)
        # ensure text is text
        if textColumn_:
            df.loc[:, textColumn_] = df.loc[:, textColumn_].astype('str')
        # replace string by "error code" and ensure float is float
        if realColumn_:
            # df.loc[:, realColumn_].replace({0: "<DL"}, inplace=True)
            for col in realColumn_:
                # print(df.loc[:, col])
                if df.loc[:, col].dtypes == "object":
                    df.loc[:, col] = df.loc[:, col].apply(pd.to_numeric, errors="coerce")


    def check(self, fileerror="database_error.xlsx"):
        """Run all checks defined for all stations

        It creates a excel file `database_errors.xlsx` with all check returns for all
        stations.
        """
        errors = {}
        for station in self.stations:
            station.check()
            if station.checker.errors:
                errors[station._name] = station.checker.errors
        self.errors = errors
        df = pd.DataFrame(errors)
        df.T.to_excel(fileerror)


class Station():
    """Measurement at a given station.

    Parameters
    ----------

    name : str
        The name of the station
    raw_data_chem : pd.DataFrame, None
        Excel sheet read by pandas with everything related to filters
    raw_data_hf : pd.DataFrame, None
        Excel sheet read by pandas with everything related to the high frequency measures
        

    Attributes
    ----------
    _name : str
        Name of the station
    _raw_data_chem : pd.DataFrame
    _raw_data_hf : pd.DataFrame
    checker : Check
    columns : list
        All columns in filters and hf dataframe
    dateColumn : list
    textColumn : list
    boolColumn : list
    realColumn : list
    OPcolumns : list

    dfChem : pd.DataFrame
    dfHF : pd.DataFrame
    df : pd.DataFrame
    df_header : pd.DataFrame


    """
    def __init__(self, name=None, raw_data_chem=None, raw_data_hf=None):
        self._name = name
        self._raw_data_chem = raw_data_chem
        self._raw_data_hf = raw_data_hf
        self.checker = Check(station=name)

        columns = []
        if self._raw_data_chem is not None:
            columns += self._raw_data_chem.columns.tolist()
        if self._raw_data_hf is not None:
            columns += self._raw_data_hf.columns.tolist()

        self.columns = columns
        self.dateColumn = [
                "Date",
                "Sampling_started_at",
                "Sampling_ended_at",
                "Date_IC-sucres",
                "Date_metals",
                "Timelist"
                ]
        self.textColumn = [
                "Sample_ID",
                "Sample_ID",
                "Commentary",
                "Programme",
                "Ville",
                "Labels",
                "Date_exacte",
                "Particle_size"
                ]
        self.boolColumn = ["Big_serie", "Blank"]
        self.realColumn = list(set(columns) - set(self.dateColumn+self.textColumn+self.boolColumn))
        self.OPcolumns = [
                "OP_DTT_µg",
                "SD_OP_DTT_µg",
                "OP_DTT_m3",
                "SD_OP_DTT_m3",
                "OP_AA_µg",
                "SD_OP_AA_µg",
                "OP_AA_m3",
                "SD_OP_AA_m3",
                "OP_DCFH_µg",
                "SD_OP_DCFH_µg",
                "OP_DCFH_m3",
                "SD_OP_DCFH_m3"
                ]

        self.dfChem = None
        self.dfHF = None
        self.df = None
        self.dfheader = None

    def build(self):
        """Parse and merge the data to some handy dataframe

        Add the following attribute :

        df : pd.DataFrame
            DataFrame of samples values
        df_blanks : pd.DataFrame
            DataFrame of blanks values
        df_units : pd.DataFrame
            DataFrame of units values
        """

        default = pd.DataFrame(
                data={
                    "Station": np.nan,
                    "Sample_ID": np.nan,
                    "Particle_size": "?",
                    "Date": np.nan,
                    "Labels": np.nan
                    },
                index=[0])\
                .set_index(["Station", "Date", "Particle_size", "Sample_ID", "Labels"])
        dfChem = default
        dfHF = default
        dfBlank = pd.DataFrame()
        df_header = pd.DataFrame()
        units = pd.DataFrame()

        # ==== Create the chemical species part
        if self._raw_data_chem is not None:
            dfChem = self._raw_data_chem

            idx_firstvalue = self.get_first_value(dfChem)

            df_header = dfChem.loc[range(0, idx_firstvalue), :]

            units = df_header.loc[0, :]
            units.name = self._name

            df_header.drop(0, inplace=True)
            df_header = df_header.assign(Station=self._name)

            dfChem = dfChem.assign(Station=self._name)
            dfChem = dfChem.loc[idx_firstvalue:]

            # If blank values are provided, separate blanks and samples
            if len(dfChem["Blank"].dropna()) > 0:
                dfChem["Blank"] = dfChem["Blank"].replace(MAP_BOOLEAN).fillna(False)
                idx_samples = dfChem["Blank"].isnull() | dfChem["Blank"]==False
                dfBlank = dfChem.loc[~idx_samples]
                dfBlank = dfBlank.loc[dfBlank["Sample_ID"].notnull()]
                dfChem = dfChem.loc[idx_samples]

            dfChem.drop("Blank", axis=1, inplace=True)

            dfChem["Date"] = pd.DatetimeIndex(dfChem["Date"])

            self.ensure_particle_size_is_set(dfChem)

            if "Labels" not in dfChem.columns:
                dfChem["Labels"] = np.nan

            dfChem.set_index(
                ["Station", "Date", "Particle_size", "Sample_ID", "Labels"],
                inplace=True
            )

        # ==== Create the high frequencies data part
        if self._raw_data_hf is not None:
            dfHF = self._raw_data_hf
            dfHF = dfHF.assign(Station=self._name)
            dfHF = dfHF[self.hasDate(dfHF["Date"])]

            dfHF["Date"] = pd.to_datetime(dfHF["Date"])
            self.ensure_particle_size_is_set(dfHF)

            if "Labels" not in dfHF.columns:
                dfHF["Labels"] = np.nan

            dfHF.set_index(
                    ["Station", "Date", "Particle_size", "Sample_ID", "Labels"],
                    inplace=True
                    )

        # ==== Merge dataframes
        # Not possible any more, too many data in dfHF.
        # df = dfChem.join(dfHF, how="outer", rsuffix="_hf", sort=False)\
        #         .dropna(axis=0, how='all')
        df = dfChem

        # df.index.names = ["Station", "Date", "Sample_ID", "Labels", "Particle_size"]
        isDate = self.hasDate(df.index.get_level_values("Date"))
        df = df[isDate]
        # Check
        self.replace_DLandQL(df)
        self.ensure_value_dtype(df, self.dateColumn, self.boolColumn, self.realColumn, self.textColumn)
        self.replace_DLandQL(dfBlank)
        self.ensure_value_dtype(dfBlank, self.dateColumn, self.boolColumn, self.realColumn, self.textColumn)
        # self.ensure_value_dtype(df_header, self.dateColumn, self.boolColumn, self.realColumn, self.textColumn)
        # Check.check_positivity(dfTmp, realColumn)
        # PM10 recons
        _ = self.add_PMreconstructed(df, takeOnlyPM10=False)
        _ = self.add_ionic_balance(df)
        
        self.df = df
        self.df_units = units
        self.df_header = df_header
        self.df_blanks = dfBlank
        self.df_hf = dfHF
        
    def merge_duplicated_columns(self, df):
        """Merge 2 duplicated columns

        :df: TODO
        :returns: TODO

        """
        duplicated = set(df.columns[df.columns.duplicated()])
        if len(duplicated) == 0:
            return
        for dup in duplicated:
            try:
                dfduplicated = df[dup].copy()
                merged = dfduplicated.iloc[:, 0]
                for i in range(1, len(dfduplicated.columns)):
                    merged = merged.combine_first(dfduplicated.iloc[:, i])
                df.drop(dup, axis="columns", inplace=True)
                df[dup] = merged
            except:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                raise ValueError("Error in {} for column: {} (duplicated)\n{}".format(self._name, dup, exc_value))

    def get_first_value(self, df):
        """
        Assume that df has a Date column
        """
        try:
            if "Date" in df.columns:
                first_value = np.where(df["Date"].notnull())[0][0]
            elif "Date" in df.index.names:
                first_value = np.where(df.index.get_level_value("Date").notnull())[0][0]
            else:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                raise ValueError("Cannot get valid index (no Date) for {}\n{}".format(self._name, exc_value))
        except IndexError:
            print("WARNING: No 'Date' for {}".format(self._name))
            header = ("QL", "DL", "U%", "CV")
            try:
                header_idx = df["Sample_ID"].str.startswith(header)
                first_value = np.where(header_idx)[0][-1]+1
            except IndexError:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                raise IndexError("Tout semble être un header pour {}\n{}\n{}".format(self._name, df.header(), exc_value))

        return first_value

    def ensure_particle_size_is_set(self, df):
        """Set the particle size to one of PM10, PM2.5, PM1 or ?

        :df: DataFrame with a 'Particle_size' column
        """
        # replace PM2,5 by PM2.5
        df["Particle_size"] = df["Particle_size"].str.replace(",", ".")
        # replace empty by ?
        df["Particle_size"] = df["Particle_size"].fillna("?")

    def hasDate(self, c):
        return pd.notnull(pd.to_datetime(c, errors="coerce"))

    def add_PMreconstructed(self, df, takeOnlyPM10=True):
        """
        Add a column `PM10recons`: the mass of the PM10 according to the chemistry

        Returns
        -------
        PMrecons: pd.DataFrame
        """

        if takeOnlyPM10 and ("PM10" not in df.index.get_level_values("Particle_size").unique()):
            print(
                "WARNING: no 'PM10' in 'Particle size'. Cannot reconstruct the PM "
                "mass."
            )
            return

        # The reconstruction method hold only for PM10.
        if takeOnlyPM10:
            idx = df.index.get_level_values("Particle_size") == "PM10"
            dftmp = df.loc[idx].copy()
        else:
            dftmp = df.copy()

        if "Date" not in df.index.names:
            dftmp.set_index(["Date"], inplace=True)

        num = dftmp._get_numeric_data()
        num[num < 0] = 0
        dftmp.dropna(axis=1, how="all", inplace=True)
        cols = dftmp.columns

        PMrecons = pd.DataFrame(index=dftmp.index, columns=[])

        required_cols = ["Na+", "Ca2+", "OC", "EC", "NH4+", "NO3-", "SO42-"]
        for c in required_cols:
            if c not in cols:
                print("WARNING: PM reconstrution fail, missing {c}".format(c=c))
                return
        nancol = pd.isna(df[required_cols]).any(axis=1)

        if "NO3-" in cols:
            PMrecons["NO3-"] = dftmp["NO3-"]/1000
        if "NH4+" in cols:
            PMrecons["NH4+"] = dftmp["NH4+"]/1000
        if "EC" in cols:
            PMrecons["EC"] = dftmp["EC"]
        if "OC" in cols:
            PMrecons["OM"] = 1.75*dftmp["OC"]
        if "SO42-" in cols:
            ssSO4 = 0.252 * dftmp["Na+"]
            nssSO4 = dftmp["SO42-"] - ssSO4
            PMrecons["nssSO4"] = nssSO4/1000

        # seasalt
        if "Cl-" in cols:
            seasalt = dftmp["Cl-"] + 1.47*dftmp["Na+"]
        else:
            seasalt = 2.252 * dftmp["Na+"]
        PMrecons["seasalt"] = seasalt/1000

        # ==== dust ===============================================================
        # Putaud : Putaud et al (2003a)
        #    dust = 4.6 * nssCa
        #         = 4.6 * (Ca2+ - Na+/26)
        # Malm : Malm et al (1993)
        #    dust = 0.16 * (1.90*Al + 2.15*Si + 1.41*Ca + 1.09*Fe + 1.67*Ti)
        # Querol/Perez : Querol et al (2000) & Pérez et al (2008)
        #    dust = Al2O3 + CO3 + SiO2
        #         = 1.89*Al + 1.5*Ca + 3*(1.89*Al)
        maskPutaud = pd.Series(index=dftmp.index, data=[True]*len(dftmp))
        maskMalm = pd.Series(index=dftmp.index, data=[True]*len(dftmp))
        maskQuerolPerez = pd.Series(index=dftmp.index, data=[True]*len(dftmp))
        dust = pd.DataFrame(
            columns=['dust'], data=np.nan, index=dftmp.index
        )

        # (Putaud et al., 2003a)
        maskPutaud = dftmp["Ca2+"].notnull() & dftmp["Na+"].notnull()
        nssCa = dftmp["Ca2+"] - dftmp["Na+"]/26
        dusttmp = 4.6 * nssCa
        dust.loc[maskPutaud, "dust"] = dusttmp[maskPutaud]
        # (Malm et al., 1993)
        if all([i in cols for i in ["Al", "Si", "Ca", "Fe", "Ti"]]):
            for c in ["Al", "Si", "Ca", "Fe", "Ti"]:
                maskMalm = maskMalm & dftmp[c].notnull()
            dusttmp = 0.16 * (1.90*dftmp["Al"] + 2.15*dftmp["Si"] + 1.41*dftmp["Ca"]
                              + 1.09*dftmp["Fe"] + 1.67*dftmp["Ti"])
            dust.loc[maskMalm, "dust"] = dusttmp[maskMalm]
        else:
            maskMalm = ~maskMalm
        # (Querol et al., 2000; Pérez et al., 2008)
        querolperez_metals = ["Al", "Ca", "Fe", "K", "Mg", "Mn", "Ti", "P"]
        if all([i in cols for i in querolperez_metals]):
            maskQuerolPerez = maskQuerolPerez & dftmp[querolperez_metals].notnull().all(axis=1)
            Al2O3 = 1.89 * dftmp["Al"]
            CO3 = 1.5 * dftmp["Ca"]
            SiO2 = 3 * Al2O3
            dusttmp = Al2O3 + CO3 + SiO2
            for c in ["Ca", "Fe", "K", "Mg", "Mn", "Ti", "P"]:
                dusttmp += dftmp[c]
            dust.loc[maskQuerolPerez, "dust"] = dusttmp[maskQuerolPerez]
        else:
            maskQuerolPerez = ~maskQuerolPerez

        PMrecons["dust"] = dust["dust"]

        PMreconsType = pd.DataFrame(index=PMrecons.index,
                                    columns=["dust_recons_type"], data="None")
        PMreconsType.where(~maskPutaud, other="Putaud et al. 2003a", inplace=True)
        PMreconsType.where(~maskMalm, other="Malm et al, 1993", inplace=True)
        PMreconsType.where(~maskQuerolPerez, other="Querol et al., 2000 ; Pérez et al., 2008", inplace=True)
        PMrecons["dust"] /= 1000

        # ==== non dust (Salameh et al. 2014)
        nondust_metals = ["Cu", "Ni", "Pb", "V", "Zn"]
        if all([i in cols for i in nondust_metals]):
            nondust = dftmp[nondust_metals].sum(axis=1)
            PMrecons["nondust"] = nondust/1000

        # ==== PM recons ==========================================================
        # print(PMrecons.sum(axis=1))
        # print(df)
        df["PM10recons"] = PMrecons.sum(axis=1).replace({0: np.nan})
        df["PM10reconsDustType"] = PMreconsType
        # print(df["PM10recons"])
        df.loc[nancol, "PM10recons"] = np.nan

        return PMrecons

    def add_ionic_balance(self, df):
        """Add the ionic balance of the sample as a new column `Ionic_balance`

        Cl- + Oxalate + MSA + NO3- + SO42- = K+ + Na+ + NH4+ + Mg2+ + Ca2+

        Molar masses are adjusted.

        Parameters
        ----------

        df : pd.DataFrame
        """
        molar_mass = {
                "Cl-": 35.4527,
                "NO3-": 62.005,
                "SO42-": 96.0643,
                "Na+": 22.9898,
                "K+": 39.0983,
                "NH4+": 18.03846,
                "Mg2+": 24.3051,
                "Ca2+": 40.0789,
                "MSA": 96.11,
                "Oxalate": 88.02,
                }
        charges = {
                "Cl-": -1,
                "NO3-": -1,
                "SO42-": -2,
                "MSA": -1,
                "Oxalate": -2,
                "Na+": 1,
                "K+": 1,
                "NH4+": 1,
                "Mg2+": 2,
                "Ca2+": 2,
                }
        annions = ["Cl-", "MSA", "NO3-", "SO42-", "Oxalate"]
        cations = ["K+", "Na+", "NH4+", "Mg2+", "Ca2+"]
        balance = pd.DataFrame(index=df.index, columns=["ionic_balance", "annions", "cations",])
        for ion in annions+cations:
            if not ion in df.columns:
                continue
            balance["ionic_balance"] = (
                    balance["ionic_balance"].add(
                        ((df[ion].clip(0) / molar_mass[ion]) * charges[ion]),
                        fill_value=0
                    ))
            if ion in annions:
                balance["annions"] = (
                        balance["annions"].add(
                            ((df[ion].clip(0) / molar_mass[ion]) * charges[ion]),
                            fill_value=0
                        ))
            if ion in cations:
                balance["cations"] = (
                        balance["cations"].add(
                            ((df[ion].clip(0) / molar_mass[ion]) * charges[ion]),
                            fill_value=0
                        ))

        df["Ionic_balance"] = balance["ionic_balance"]
        df["Annions"] = balance["annions"].abs()
        df["Cations"] = balance["cations"]

    def replace_DLandQL(self, df):
        """Replace inplace <QL and <DL value by error code

        `<QL` and `<LQ` are replaced by -1
        `<DL` and `<LD` are replaced by -2

        Parameters
        ----------

        df : pd.DataFrame

        """
        QLandDL = {
            "<QL": -1, "<LQ": -1,
            "<DL": -2, "<LD": -2,
        }
        df.replace(QLandDL, inplace=True)

    def ensure_value_dtype(self, df, dateColumn, boolColumn, realColumn, textColumn):
        """Convert each column to its known dtype
        First, try to infer it.
        """
        # get columns
        dateColumn_ = set(df.columns).intersection(set(dateColumn))
        realColumn_ = set(df.columns).intersection(set(realColumn))
        textColumn_ = set(df.columns).intersection(set(textColumn))
        boolColumn_ = set(df.columns).intersection(set(boolColumn))

        # ensure date is a date
        for d in dateColumn_:
            if d in df.columns and df[d].dtypes != 'datetime64[ns]':
                try:
                    df.loc[:, d] = df[d].apply(pd.to_datetime)#pd.to_datetime(df[d])
                except Exception as e:
                    exc_type, exc_value, exc_traceback = sys.exc_info()
                    raise ValueError("Error in {} column {}\n{}".format(self._name, d, exc_value))

        # ensure bool is a bool
        if boolColumn_:
            df.loc[:,boolColumn_] = df.loc[:, boolColumn_].fillna(False).astype(bool)
        # ensure text is text
        if textColumn_:
            df.loc[:, textColumn_] = df.loc[:, textColumn_].astype('str')
        # replace string by "error code" and ensure float is float
        if realColumn_:
            # df.loc[:, realColumn_].replace({0: "<DL"}, inplace=True)
            for col in realColumn_:
                # print(df.loc[:, col])
                if df.loc[:, col].dtypes == "object":
                    df.loc[:, col] = df.loc[:, col].apply(pd.to_numeric, errors="coerce")

    def check(self):
        """Run all checks
        """
        self.checker.check_PMvalue(self.df)
        self.checker.check_positivity(self.df, self.realColumn, includeDLorQL=True)
        self.checker.check_particle_size(self.df.reset_index())
        self.checker.check_expected_range(self.df)


class Check:
    def __init__(self, station):
        self.station = station
        self.errors = {}


    def check_PMvalue(self, df):
        """Check if PM from AASQA ~ PMrecons

        :df: TODO
        :returns: TODO

        """
        if not("PM10AASQA" in df.columns and "PM10recons" in df.columns):
            return

        ratio = df["PM10AASQA"] / df["PM10recons"]
        errors = []
        if (ratio>2).any():
            errors.append(f"PM10 obs larger than 2 times PM reconstructed ({(ratio>2).sum()}/{ratio.notnull().sum()})")
        if (ratio<0.5).any():
            errors.append(f"PM10 obs lower than 2 times PM reconstructed ({(ratio<0.5).sum()}/{ratio.notnull().sum()})")
        
        if errors:
            self.errors["TEOMvsRecons"] = "\n".join(errors)

    def check_positivity(self, df, realColumn, includeDLorQL=True):
        """
        Display a warning if a nagative value is encoutered for concentration.
        """
        notPositive = set(
            ["T", "Tmin", "Tmax", "Tmean",
             "δ15Ncal(NH3)", "δ15Nscc(NO3)"
            ])
        # remove the known non positive columns
        col = list( (set(realColumn)-notPositive).intersection(set(df.columns)) )
        if len(col) == 0:
            return

        # by default, DL and QL are negative. Remove this false warning
        dfc = df.loc[:, col].dropna(how="all", axis=1).copy()
        if includeDLorQL:
            dfc.replace({-1:999, -2:999}, inplace=True)

        Nneg = (dfc<0).sum().sum()
        if Nneg>0:
            self.errors["check_positivity"] = f"Negative values encoutered ({Nneg}):\n"
            d = dfc[dfc<0].dropna(how="all").dropna(axis=1, how="all")
            for c in d.columns:
                n = d[c].notnull().sum()
                total = dfc[c].notnull().sum()
                self.errors["check_positivity"] += f"{c} ({n}/{total})\n"
                # print("{:10}: {:.2%}".format(c, d[c].notnull().sum()/dfc.shape[0]))

    def check_particle_size(self, df):
        """Particle_size should be in ["PM10", "PM2.5", "PM1"]
        """
        particle_size_allowed = ["PM10", "PM2.5", "PM1"]
        dferror = df.loc[
            ~df["Particle_size"].isin(particle_size_allowed),
            ["Station", "Sample_ID", "Particle_size"]
        ]
        unknown = set(df["Particle_size"]) - set(particle_size_allowed)
        if df["Particle_size"].isnull().any():
            unknown.add("NaN")
        if dferror.size > 0:
            self.errors["check_particle_size"] = f"Unknown particle size {unknown}"
            # print("Particle size is not in ", particle_size_allowed)
            # print(dferror)

    def check_expected_range(self, df):
        """Check if concentration values are in the range of what is expected
        """
        PM10_expected = pd.Series({
                "Al": 500,
                "NO3-": 7000,
                "SO42-": 7000,
                "OC": 20,
                "EC": 10,
                })
        cols = [c for c in PM10_expected.index if c in df.columns]

        not_nul_idx = df[cols].notnull()

        dftmp = df[cols].fillna(0).gt(PM10_expected[cols]).sum()
        
        if dftmp.sum() > 0:
            self.errors["High values"] = ""
            for specie in dftmp.index:
                if dftmp[specie] > 0:
                    n_higher = dftmp[specie]
                    d = dftmp.loc[specie]
                    n_count = df[specie].count()
                    self.errors["High values"] += f"{specie} (>{PM10_expected[specie]}): {n_higher}/{n_count}\n"


class Export:
    def __init__(self, sql_filename=None, db=None):
        """TODO: Docstring for __init__.

        :dbName: TODO
        :returns: TODO

        """
        self.sql_filename = sql_filename
        self.DataBase = db

    def export_to_table(self, df, table):
        """Export the DataFrame to the database

        df : pd.DataFrame
        con : sqlite3.connection
        table : str, name of the table

        """
        dftmp = df.copy()

        dftmp.columns = dftmp.columns.str.replace(" ", "_")

        dftmp = dftmp.reset_index().round(5)

        conn = sqlite3.connect(self.sql_filename)
        dftmp.to_sql(table, con=conn, if_exists="replace")
        conn.close()

    def export_values_to_db(self):
        # ugly, but works... TODO doest not work 3 aout 2020
        df_units = self.DataBase.df_units
        df_units.index.name = "Specie"

        # # ===== header (QL & co)
        df_QL = self.DataBase.df_QL.sort_index(level=["Station"])
        # self.ensure_value_dtype(df_header, dateColumn, boolColumn, realColumn, textColumn)
        # Check.check_positivity(df_header, realColumn, includeDLorQL=False)
        # QL = df_header[df_header["Sample_ID"].str.contains("QL")]
        # QL.reset_index().round(5).to_sql("QL", conn, if_exists="replace")
        # df_header.reset_index().round(5).to_sql("QL", conn, if_exists="replace")

        # ===== values
        df_values = self.DataBase.df\
                .sort_index(level=["Station", "Date"])\
                .sort_index(axis=1)
        # self.ensure_value_dtype(df_values, dateColumn, boolColumn, realColumn, textColumn)


        # ==== blank
        df_blanks = self.DataBase.df_blanks\
                .sort_index(level=["Station", "Date"])\
                .sort_index(axis=1)

        # ==== high frequency
        # df_hf = self.DataBase.df_hf\
        #         .sort_index(level=["Station", "Date"])

        # ===== Write to the database
        self.export_to_table(df_units, table="units")
        self.export_to_table(df_QL, table="QL")
        self.export_to_table(df_values, table="values_all")
        self.export_to_table(df_blanks, table="blanks")
        # self.export_to_table(df_hf, table="high_frequency")

    def export_to_csv(self, data, filename):
        """
        Export a DataFrame to the given file
        """
        if not os.path.exists(os.path.dirname(filename)):
            os.makedirs(os.path.dirname(filename))

        tmp = data.dropna(how="all", axis=1)\
                  .sort_index(axis=1)\
                  .infer_objects()\
                  .round(5)
        tmp.to_csv(filename, index=True)

    def export_each_to_csv(self):
        for station in self.DataBase.stations:
            self.export_to_csv(
                data=station.df,
                filename="sites/{name}/{name}_PM.csv".format(name=station._name)
            )

if __name__ == "__main__":
    db = DataBase()
    db.create()
    db.check()

    export = Export(sql_filename="aerosols.db", db=db)
    export.export_each_to_csv()
    export.export_values_to_db()

