from django.contrib import admin
from .models import RawDatabase

class RawDatabaseAdmin(admin.ModelAdmin):
    list_display = ('created_on', 'excel', 'in_use', 'check_file', 'status', 'error')

admin.site.register(RawDatabase, RawDatabaseAdmin)
