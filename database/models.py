from django.db import models
from django.utils import timezone
from django.core.files.storage import FileSystemStorage

fs = FileSystemStorage(location='media/')

# Create your models here.

class RawDatabase(models.Model):
    STATUS = [
            ('Proceed', 'Proceed'),
            ('Processing', 'Processing'),
            ('Pending', 'Pending'),
            ('Failed', 'Failed'),
            ]

    excel = models.FileField(storage=fs, upload_to="raw_database/%Y/%m/%d/")
    status = models.CharField(max_length=255, choices=STATUS, default="Pending")

    created_on = models.DateTimeField(default = timezone.now)
    check_file = models.FileField(storage=fs, upload_to="raw_database/%Y/%m/%d/", blank=True)
    error = models.TextField(null=True, blank=True)
    in_use = models.BooleanField(default=False, blank=False)

    def __str__(self):
        return f"Database {self.created_on}"
