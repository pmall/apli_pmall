import sys
from django.core.management.base import BaseCommand
from django.core.files import File
from django.conf import settings
from datetime import datetime

from database.models import RawDatabase

from database.libDataBase import excel2sql


class Command(BaseCommand):
    help = "Auto import Database excel file"

    def handle(self, **options):
        for database in RawDatabase.objects.filter(status='Pending'):
            database.status = 'Processing'
            database.error = ""
            database.save()

            if not database.excel:
                database.error = "ERROR: no database"
                database.status = "Failed"
            else:
                try:
                    db = excel2sql.DataBase(filePM=f'{settings.MEDIA_ROOT}/{database.excel}')
                    db.create()
                except Exception as e:
                    exc_type, exc_value, exc_traceback = sys.exc_info()
                    database.error = exc_value
                    database.status = 'Failed'
                else:
                    try:
                        export = excel2sql.Export(sql_filename="aerosols.db", db=db)
                        export.export_values_to_db()
                    except Exception as e:
                        exc_type, exc_value, exc_traceback = sys.exc_info()
                        database.error = exc_value
                        database.status = 'Failed'
                    else:
                        try:
                            filepath = "{base}/raw_database/{date}/database_check.xlsx".format(
                                    base=settings.MEDIA_ROOT,
                                    date=datetime.now().strftime("%Y/%m/%d")
                                    )
                            print(filepath)
                            try:
                                db.check(fileerror=filepath)
                            except Exception as e:
                                print(e)

                            try:
                                database.check_file.name = "raw_database/{date}/database_check.xlsx".format( date=datetime.now().strftime("%Y/%m/%d"))
                            except Exception as e:
                                print(e)
                        except:
                            database.error = "Check failed"
                            database.status = 'Proceed'
                            RawDatabase.objects.filter(in_use=True).update(in_use=False)
                            database.in_use = True
                        else:
                            database.error = ""
                            database.status = 'Proceed'
                            RawDatabase.objects.filter(in_use=True).update(in_use=False)
                            database.in_use = True
            database.save()
